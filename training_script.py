# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 22:02:17 2021

Some of the Code is from the Tensorflow Authors
https://www.tensorflow.org/tutorials/generative/cyclegan

This Script is for training 8 Cycle-GANs used for the Simulation of Wear and Corrosion of ancient coins.
Attribut Changes are based on the Swiss Scale (https://www.coinfinds.ch/services/supplement/introduction.html)
1 Dataset for each of the 5 Stages per Attribut are required -> 10 Datasets total as .Pickle Savefile

requires models.py
"""
import tensorflow as tf
import os
import models
import pickle
import time
import matplotlib.pyplot as plt
from IPython.display import clear_output
import numpy as np


#Number of trainsteps until new parameter optimizations 
BATCH_SIZE = 1

#Balancing Parameter -> handles influence of identity loss and cycle loss for generator optimization
LAMBDA = 10

class cycle_gan_net(object):
    
    def __init__(self, data_set_x, data_set_y, name):
        self.name = name
        
        self.loss_obj = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        
        self.get_checkpoints()
        self.prepare_data(data_set_x,data_set_y)
        
    def prepare_data(self,data_set_x,data_set_y):
        """
        Data preparation -> form images into tensors, normalize and shuffle them
        """
        self.data_set_x = tf.data.Dataset.from_tensor_slices(data_set_x)
        self.data_set_y = tf.data.Dataset.from_tensor_slices(data_set_y)
        
        self.data_set_x = self.data_set_x.map(
                self.normalize, num_parallel_calls=tf.data.AUTOTUNE).cache().shuffle(
                1000).batch(BATCH_SIZE)

        self.data_set_y = self.data_set_y.map(
                self.normalize, num_parallel_calls=tf.data.AUTOTUNE).cache().shuffle(
                1000).batch(BATCH_SIZE)
                    
    def normalize(self,image):
        """
        normalize images into [-1,1]
        """
        image = tf.cast(image, tf.float32)
        image = (image / 127.5) - 1
        return image
        
        
    def get_checkpoints(self):
        """
        create Models and Optimizer
        Load Checkpoints if available
        """
        self.generator_g = models.Generator()
        self.generator_f = models.Generator()
        
        self.discriminator_x = models.Discriminator()
        self.discriminator_y = models.Discriminator()
        
        self.generator_g_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        self.generator_f_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

        self.discriminator_x_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        self.discriminator_y_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        
        self.model_epochs = 0
        
        checkpoint_path = "./checkpoints/train/"+self.name
        
        ckpt = tf.train.Checkpoint(generator_g=self.generator_g,
                           generator_f=self.generator_f,
                           discriminator_x=self.discriminator_x,
                           discriminator_y=self.discriminator_y,
                           generator_g_optimizer=self.generator_g_optimizer,
                           generator_f_optimizer=self.generator_f_optimizer,
                           discriminator_x_optimizer=self.discriminator_x_optimizer,
                           discriminator_y_optimizer=self.discriminator_y_optimizer)

        self.ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=1)
        
        # if a checkpoint exists, restore the latest checkpoint.
        if self.ckpt_manager.latest_checkpoint:
            
            for filename in os.listdir(checkpoint_path):
                index = ""
                if(".index" in filename):
                    for letter in filename:   
                        try:
                            index += str(int(letter))
                        except ValueError:
                            continue
            self.model_epochs = int(index)
            
            ckpt.restore(self.ckpt_manager.latest_checkpoint)
            print ('Epoch {} restored'.format(self.model_epochs))
        
        
    def discriminator_loss(self,real_img, fake_img):
        """
        calculate loss for Discriminator with both Discriminator outputs
        real_loss = difference of disc. output from real image and array filled with 1s
        generated_loss = difference of disc. output from generated image and array filled with 0s
        """
        real_loss = self.loss_obj(tf.ones_like(real_img), real_img)
    
        generated_loss = self.loss_obj(tf.zeros_like(fake_img), fake_img)
    
        total_disc_loss = real_loss + generated_loss
    
        return total_disc_loss * 0.5
    
    def generator_loss(self,fake_img):
        """
        calculate difference of discriminator output for generated image and array filled with 1s
        -> used as generator loss
        """
        return self.loss_obj(tf.ones_like(fake_img), fake_img)
    
    def calc_cycle_loss(self,real_image, cycled_image):
        """
        calculate difference of real and cycled image for cycle loss
        
        """
        loss1 = tf.reduce_mean(tf.abs(real_image - cycled_image))
        
        #calc loss gets weight of LAMDA
        return LAMBDA * loss1
    
    def identity_loss(self,real_image, same_image):
        """
        calculate difference of real_image and same_image for identity loss
        """
        loss = tf.reduce_mean(tf.abs(real_image - same_image))
        
        #id loss gets weight of LAMDA/2
        return LAMBDA * 0.5 * loss
    
    
      
      
    def generate_images(self,model, test_input):
        """
        output train result of recent trainepoch
        """
        to_show = model(test_input)
    
        plt.figure(figsize=(12, 6),dpi=200)
    
        display_list = [test_input[0], to_show[0]]
        title = ['Input Image', 'Predicted Image']
    
        for i in range(2):
            plt.suptitle("Model: "+self.name+" | Epoch: "+str(self.model_epochs))
            plt.subplot(1, 2, i+1)
            plt.title(title[i])
            plt.imshow(display_list[i] * 0.5 + 0.5)
            plt.axis('off')
        plt.show()
      
    @tf.function
    def train_step(self,real_x, real_y):
      """
      Train step contains all loss value calculations and optimizations for one learn step
      needs one image of each dataset
      """
      with tf.GradientTape(persistent=True) as tape:
          #G(X) -> first Simulation
          fake_y = self.generator_g(real_x, training=True)
          #F(G(X)) -> cycled_x used for cycle loss
          cycled_x = self.generator_f(fake_y, training=True)
          
          #F(Y) -> second Simulation
          fake_x = self.generator_f(real_y, training=True)
          #G(F(Y)) -> cycled_y used for cycle loss
          cycled_y = self.generator_g(fake_x, training=True)
    
          #F(X) -> same x for id loss
          same_x = self.generator_f(real_x, training=True)
          #G(Y) -> same y for id loss
          same_y = self.generator_g(real_y, training=True)
          
          #D_X(X) -> disc prediction of X
          disc_real_x = self.discriminator_x(real_x, training=True)
          #D_Y(Y) -> disc prediction of Y
          disc_real_y = self.discriminator_y(real_y, training=True)
          
          #D_X(F(Y)) -> disc prediction of simulated X
          disc_fake_x = self.discriminator_x(fake_x, training=True)
          #D_Y(F(X)) -> disc prediction of simulated Y
          disc_fake_y = self.discriminator_y(fake_y, training=True)
          
          #calculate generator loss with disc. prediction of generated imgs
          gen_g_loss = self.generator_loss(disc_fake_y)
          gen_f_loss = self.generator_loss(disc_fake_x)
          
          #calculated cycle loss
          total_cycle_loss = self.calc_cycle_loss(real_x, cycled_x) + self.calc_cycle_loss(real_y, cycled_y)
          #monitor cycle loss
          self.tc_loss(total_cycle_loss)
    
          #Total generator loss = adversarial loss + LAMDA * cycle loss + LAMBDA * 0.5 * id loss
          total_gen_g_loss = gen_g_loss + total_cycle_loss + self.identity_loss(real_y, same_y)
          total_gen_f_loss = gen_f_loss + total_cycle_loss + self.identity_loss(real_x, same_x)
          #monitor total generator loss
          self.g_loss(total_gen_g_loss)
          self.f_loss(total_gen_f_loss)
        
          disc_x_loss = self.discriminator_loss(disc_real_x, disc_fake_x)
          disc_y_loss = self.discriminator_loss(disc_real_y, disc_fake_y)
          #monitor discriminator loss
          self.x_loss(disc_x_loss)
          self.y_loss(disc_y_loss)
    
      #calculate the gradients for generator and discriminator
      generator_g_gradients = tape.gradient(total_gen_g_loss, 
                                            self.generator_g.trainable_variables)
      generator_f_gradients = tape.gradient(total_gen_f_loss, 
                                            self.generator_f.trainable_variables)
    
      discriminator_x_gradients = tape.gradient(disc_x_loss, 
                                                self.discriminator_x.trainable_variables)
      discriminator_y_gradients = tape.gradient(disc_y_loss, 
                                                self.discriminator_y.trainable_variables)
    
      #apply the gradients to the optimizer
      self.generator_g_optimizer.apply_gradients(zip(generator_g_gradients, 
                                                self.generator_g.trainable_variables))
    
      self.generator_f_optimizer.apply_gradients(zip(generator_f_gradients, 
                                                self.generator_f.trainable_variables))
    
      self.discriminator_x_optimizer.apply_gradients(zip(discriminator_x_gradients,
                                                    self.discriminator_x.trainable_variables))
    
      self.discriminator_y_optimizer.apply_gradients(zip(discriminator_y_gradients,
                                                    self.discriminator_y.trainable_variables))
      
    
    
    def tensorboard(self):
        """
        prepare tensorboard file -> used to monitor the loss values
        """
        self.g_loss = tf.keras.metrics.Mean('g_loss', dtype=tf.float32)
        self.f_loss = tf.keras.metrics.Mean('f_loss', dtype=tf.float32)
        self.x_loss = tf.keras.metrics.Mean('x_loss', dtype=tf.float32) 
        self.y_loss = tf.keras.metrics.Mean('y_loss', dtype=tf.float32)
        self.tc_loss = tf.keras.metrics.Mean('tc_loss', dtype=tf.float32)
        
        self.summary_writer = tf.summary.create_file_writer('logs/{}'.format(self.name))
        
    def main_loop(self, EPOCHS):
        """
        main loop for Training Process
        Calls the Train Step function for each EPOCHS and gives 1 element of each Dataset
        """
        example_img = next(iter(self.data_set_x))
        steps = min(len(self.data_set_x),len(self.data_set_y))
        
        #start for-loop for each epoch
        for epoch in range(EPOCHS):
            self.model_epochs += 1
            start = time.time()
            
          
            counter = 0
            #create as many pairs as possible with 1 element per dataset -> each element used only once -> iterate through all
            for image_x, image_y in tf.data.Dataset.zip((self.data_set_x, self.data_set_y)):
                self.train_step(image_x, image_y)
                print("\r"+"Epoch: {} Progress: {}/{}".format(self.model_epochs,counter,steps), end="")
                counter+=1
            
            #monitor last result of trainingepoch
            clear_output(wait=True)
            img = self.generator_g(example_img)
            img = np.reshape(img,(-1,256,256,3))
            img = img*0.5+0.5
            
            #monitor last values of trainingepoch
            with self.summary_writer.as_default():
                tf.summary.scalar('total_cycle_loss', self.tc_loss.result(), step=self.model_epochs)
                tf.summary.scalar('g_loss', self.g_loss.result(), step=self.model_epochs)
                tf.summary.scalar('f_loss', self.f_loss.result(), step=self.model_epochs)
                tf.summary.scalar('x_loss', self.x_loss.result(), step=self.model_epochs)
                tf.summary.scalar('y_loss', self.y_loss.result(), step=self.model_epochs)
                tf.summary.image("Result", img, step=self.model_epochs)
                
            self.tc_loss.reset_states()
            self.g_loss.reset_states()
            self.f_loss.reset_states()
            self.x_loss.reset_states()
            self.y_loss.reset_states()
          
                
            #print last result of trainingepoch
            self.generate_images(self.generator_g, example_img)
          
            if (self.model_epochs) % 1 == 0:
              ckpt_save_path = self.ckpt_manager.save()
              print ('Saving checkpoint for epoch {} at {}'.format(self.model_epochs,
                                                                   ckpt_save_path))
          
            print ('Time taken for epoch {} is {} sec\n'.format(self.model_epochs,
                                                                time.time()-start))
  
##############################################################################################################

"""
This part controls the Training of each Cycle-GAN
Every Cycle-GAN gets loaded one after the other to save memory
Number of Epochs can be changed
"""


NAME = ["Usage_1_2","Usage_2_3","Usage_3_4","Usage_4_5","Corrosion_1_2","Corrosion_2_3","Corrosion_3_4","Corrosion_4_5"]
EPOCHS = 100
usage_data_sets = []
corrosion_data_sets = []

# Load Datasets
for i in range(5):
    usage_data_sets.append(pickle.load(open("A_"+str(i+1)+".pickle","rb")))
    corrosion_data_sets.append(pickle.load(open("K_"+str(i+1)+".pickle","rb")))


# Train Usage Nets
for i in range(4):
    net = cycle_gan_net(usage_data_sets[i],usage_data_sets[i+1],"CycleNet_"+NAME[i])
    net.tensorboard()
    net.main_loop(EPOCHS)
    
# Train Corrosion Nets
for i in range(4):
    net = cycle_gan_net(corrosion_data_sets[i],corrosion_data_sets[i+1],"CycleNet_"+NAME[i+4])
    net.tensorboard()
    net.main_loop(EPOCHS)
