# CoinChanger

Simulation of wear and corrosion of ancient coins using Cycle Generative Adversarial Networks method.

- models.py contains ML Models for Cycle-GAN
- training_script.py is used for training
- GUI contains models.py + tkinter GUI with pre-trained Nets
    - GUI.py to start GUI
    - folder checkpoints contains 8 pre-trained Cycle-GANs

# System Requirements

All scripts are written and tested in Windows 10 using Python 3.6.
The following packages are necessary:

1. Tensorflow 2.5
2. Numpy 1.19.5
3. Pickle - (other methods for Data-Handling possible)
4. Matplotlib 3.3.4

For the GUI are also required:

5. Tkinter 
6. Pillow 8.2
7. OpenCV 4.5
