¦Ļ
ŲØ
D
AddV2
x"T
y"T
z"T"
Ttype:
2	
B
AssignVariableOp
resource
value"dtype"
dtypetype
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype

Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

.
Identity

input"T
output"T"	
Ttype
\
	LeakyRelu
features"T
activations"T"
alphafloat%ĶĢL>"
Ttype0:
2

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
?
Mul
x"T
y"T
z"T"
Ttype:
2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
.
Rsqrt
x"T
y"T"
Ttype:

2
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
G
SquaredDifference
x"T
y"T
z"T"
Ttype:

2	
¾
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
@
StaticRegexFullMatch	
input

output
"
patternstring
2
StopGradient

input"T
output"T"	
Ttype
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
Sub
x"T
y"T
z"T"
Ttype:
2	

VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 "serve*2.5.02v2.5.0-rc3-213-ga4dfb8d1a718®Ģ

conv2d_136/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameconv2d_136/kernel

%conv2d_136/kernel/Read/ReadVariableOpReadVariableOpconv2d_136/kernel*(
_output_shapes
:*
dtype0

 instance_normalization_229/scaleVarHandleOp*
_output_shapes
: *
dtype0*
shape:*1
shared_name" instance_normalization_229/scale

4instance_normalization_229/scale/Read/ReadVariableOpReadVariableOp instance_normalization_229/scale*
_output_shapes	
:*
dtype0

!instance_normalization_229/offsetVarHandleOp*
_output_shapes
: *
dtype0*
shape:*2
shared_name#!instance_normalization_229/offset

5instance_normalization_229/offset/Read/ReadVariableOpReadVariableOp!instance_normalization_229/offset*
_output_shapes	
:*
dtype0

conv2d_137/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameconv2d_137/kernel

%conv2d_137/kernel/Read/ReadVariableOpReadVariableOpconv2d_137/kernel*'
_output_shapes
:*
dtype0
v
conv2d_137/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_nameconv2d_137/bias
o
#conv2d_137/bias/Read/ReadVariableOpReadVariableOpconv2d_137/bias*
_output_shapes
:*
dtype0

conv2d_133/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*"
shared_nameconv2d_133/kernel

%conv2d_133/kernel/Read/ReadVariableOpReadVariableOpconv2d_133/kernel*&
_output_shapes
:@*
dtype0

conv2d_134/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*"
shared_nameconv2d_134/kernel

%conv2d_134/kernel/Read/ReadVariableOpReadVariableOpconv2d_134/kernel*'
_output_shapes
:@*
dtype0

 instance_normalization_227/scaleVarHandleOp*
_output_shapes
: *
dtype0*
shape:*1
shared_name" instance_normalization_227/scale

4instance_normalization_227/scale/Read/ReadVariableOpReadVariableOp instance_normalization_227/scale*
_output_shapes	
:*
dtype0

!instance_normalization_227/offsetVarHandleOp*
_output_shapes
: *
dtype0*
shape:*2
shared_name#!instance_normalization_227/offset

5instance_normalization_227/offset/Read/ReadVariableOpReadVariableOp!instance_normalization_227/offset*
_output_shapes	
:*
dtype0

conv2d_135/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameconv2d_135/kernel

%conv2d_135/kernel/Read/ReadVariableOpReadVariableOpconv2d_135/kernel*(
_output_shapes
:*
dtype0

 instance_normalization_228/scaleVarHandleOp*
_output_shapes
: *
dtype0*
shape:*1
shared_name" instance_normalization_228/scale

4instance_normalization_228/scale/Read/ReadVariableOpReadVariableOp instance_normalization_228/scale*
_output_shapes	
:*
dtype0

!instance_normalization_228/offsetVarHandleOp*
_output_shapes
: *
dtype0*
shape:*2
shared_name#!instance_normalization_228/offset

5instance_normalization_228/offset/Read/ReadVariableOpReadVariableOp!instance_normalization_228/offset*
_output_shapes	
:*
dtype0

NoOpNoOp
Ų:
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*:
value:B: B’9

layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer-7
	layer-8

layer_with_weights-5

layer-9
	variables
regularization_losses
trainable_variables
	keras_api

signatures

_init_input_shape

layer_with_weights-0
layer-0
layer-1
	variables
regularization_losses
trainable_variables
	keras_api
­
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer-2
	variables
regularization_losses
trainable_variables
	keras_api
­
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
 layer-2
!	variables
"regularization_losses
#trainable_variables
$	keras_api
R
%	variables
&regularization_losses
'trainable_variables
(	keras_api
^

)kernel
*	variables
+regularization_losses
,trainable_variables
-	keras_api
i
	.scale

/offset
0	variables
1regularization_losses
2trainable_variables
3	keras_api
R
4	variables
5regularization_losses
6trainable_variables
7	keras_api
R
8	variables
9regularization_losses
:trainable_variables
;	keras_api
h

<kernel
=bias
>	variables
?regularization_losses
@trainable_variables
A	keras_api
V
B0
C1
D2
E3
F4
G5
H6
)7
.8
/9
<10
=11
 
V
B0
C1
D2
E3
F4
G5
H6
)7
.8
/9
<10
=11
­
	variables
Inon_trainable_variables
regularization_losses
Jlayer_metrics
Kmetrics

Llayers
Mlayer_regularization_losses
trainable_variables
 
 
^

Bkernel
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
R
R	variables
Sregularization_losses
Ttrainable_variables
U	keras_api

B0
 

B0
­
	variables
Vnon_trainable_variables
regularization_losses
Wlayer_metrics
Xmetrics

Ylayers
Zlayer_regularization_losses
trainable_variables
^

Ckernel
[	variables
\regularization_losses
]trainable_variables
^	keras_api
i
	Dscale

Eoffset
_	variables
`regularization_losses
atrainable_variables
b	keras_api
R
c	variables
dregularization_losses
etrainable_variables
f	keras_api

C0
D1
E2
 

C0
D1
E2
­
	variables
gnon_trainable_variables
regularization_losses
hlayer_metrics
imetrics

jlayers
klayer_regularization_losses
trainable_variables
^

Fkernel
l	variables
mregularization_losses
ntrainable_variables
o	keras_api
i
	Gscale

Hoffset
p	variables
qregularization_losses
rtrainable_variables
s	keras_api
R
t	variables
uregularization_losses
vtrainable_variables
w	keras_api

F0
G1
H2
 

F0
G1
H2
­
!	variables
xnon_trainable_variables
"regularization_losses
ylayer_metrics
zmetrics

{layers
|layer_regularization_losses
#trainable_variables
 
 
 
Æ
%	variables
}non_trainable_variables
&regularization_losses
~layer_metrics
metrics
layers
 layer_regularization_losses
'trainable_variables
][
VARIABLE_VALUEconv2d_136/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE

)0
 

)0
²
*	variables
non_trainable_variables
+regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
,trainable_variables
ki
VARIABLE_VALUE instance_normalization_229/scale5layer_with_weights-4/scale/.ATTRIBUTES/VARIABLE_VALUE
mk
VARIABLE_VALUE!instance_normalization_229/offset6layer_with_weights-4/offset/.ATTRIBUTES/VARIABLE_VALUE

.0
/1
 

.0
/1
²
0	variables
non_trainable_variables
1regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
2trainable_variables
 
 
 
²
4	variables
non_trainable_variables
5regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
6trainable_variables
 
 
 
²
8	variables
non_trainable_variables
9regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
:trainable_variables
][
VARIABLE_VALUEconv2d_137/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
YW
VARIABLE_VALUEconv2d_137/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE

<0
=1
 

<0
=1
²
>	variables
non_trainable_variables
?regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
@trainable_variables
MK
VARIABLE_VALUEconv2d_133/kernel&variables/0/.ATTRIBUTES/VARIABLE_VALUE
MK
VARIABLE_VALUEconv2d_134/kernel&variables/1/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUE instance_normalization_227/scale&variables/2/.ATTRIBUTES/VARIABLE_VALUE
][
VARIABLE_VALUE!instance_normalization_227/offset&variables/3/.ATTRIBUTES/VARIABLE_VALUE
MK
VARIABLE_VALUEconv2d_135/kernel&variables/4/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUE instance_normalization_228/scale&variables/5/.ATTRIBUTES/VARIABLE_VALUE
][
VARIABLE_VALUE!instance_normalization_228/offset&variables/6/.ATTRIBUTES/VARIABLE_VALUE
 
 
 
F
0
1
2
3
4
5
6
7
	8

9
 

B0
 

B0
²
N	variables
non_trainable_variables
Oregularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
Ptrainable_variables
 
 
 
²
R	variables
 non_trainable_variables
Sregularization_losses
”layer_metrics
¢metrics
£layers
 ¤layer_regularization_losses
Ttrainable_variables
 
 
 

0
1
 

C0
 

C0
²
[	variables
„non_trainable_variables
\regularization_losses
¦layer_metrics
§metrics
Ølayers
 ©layer_regularization_losses
]trainable_variables

D0
E1
 

D0
E1
²
_	variables
Ŗnon_trainable_variables
`regularization_losses
«layer_metrics
¬metrics
­layers
 ®layer_regularization_losses
atrainable_variables
 
 
 
²
c	variables
Ænon_trainable_variables
dregularization_losses
°layer_metrics
±metrics
²layers
 ³layer_regularization_losses
etrainable_variables
 
 
 

0
1
2
 

F0
 

F0
²
l	variables
“non_trainable_variables
mregularization_losses
µlayer_metrics
¶metrics
·layers
 ølayer_regularization_losses
ntrainable_variables

G0
H1
 

G0
H1
²
p	variables
¹non_trainable_variables
qregularization_losses
ŗlayer_metrics
»metrics
¼layers
 ½layer_regularization_losses
rtrainable_variables
 
 
 
²
t	variables
¾non_trainable_variables
uregularization_losses
ælayer_metrics
Ąmetrics
Įlayers
 Ālayer_regularization_losses
vtrainable_variables
 
 
 

0
1
 2
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

serving_default_input_imagePlaceholder*1
_output_shapes
:’’’’’’’’’*
dtype0*&
shape:’’’’’’’’’

StatefulPartitionedCallStatefulPartitionedCallserving_default_input_imageconv2d_133/kernelconv2d_134/kernel instance_normalization_227/scale!instance_normalization_227/offsetconv2d_135/kernel instance_normalization_228/scale!instance_normalization_228/offsetconv2d_136/kernel instance_normalization_229/scale!instance_normalization_229/offsetconv2d_137/kernelconv2d_137/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8 *-
f(R&
$__inference_signature_wrapper_151718
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
Ł
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename%conv2d_136/kernel/Read/ReadVariableOp4instance_normalization_229/scale/Read/ReadVariableOp5instance_normalization_229/offset/Read/ReadVariableOp%conv2d_137/kernel/Read/ReadVariableOp#conv2d_137/bias/Read/ReadVariableOp%conv2d_133/kernel/Read/ReadVariableOp%conv2d_134/kernel/Read/ReadVariableOp4instance_normalization_227/scale/Read/ReadVariableOp5instance_normalization_227/offset/Read/ReadVariableOp%conv2d_135/kernel/Read/ReadVariableOp4instance_normalization_228/scale/Read/ReadVariableOp5instance_normalization_228/offset/Read/ReadVariableOpConst*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *(
f#R!
__inference__traced_save_152376
ä
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameconv2d_136/kernel instance_normalization_229/scale!instance_normalization_229/offsetconv2d_137/kernelconv2d_137/biasconv2d_133/kernelconv2d_134/kernel instance_normalization_227/scale!instance_normalization_227/offsetconv2d_135/kernel instance_normalization_228/scale!instance_normalization_228/offset*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *+
f&R$
"__inference__traced_restore_152422ä
Ä	
Ń
J__inference_sequential_243_layer_call_and_return_conditional_losses_151968

inputsC
)conv2d_133_conv2d_readvariableop_resource:@
identity¢ conv2d_133/Conv2D/ReadVariableOp¶
 conv2d_133/Conv2D/ReadVariableOpReadVariableOp)conv2d_133_conv2d_readvariableop_resource*&
_output_shapes
:@*
dtype02"
 conv2d_133/Conv2D/ReadVariableOpĘ
conv2d_133/Conv2DConv2Dinputs(conv2d_133/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2
conv2d_133/Conv2D¢
leaky_re_lu_132/LeakyRelu	LeakyReluconv2d_133/Conv2D:output:0*1
_output_shapes
:’’’’’’’’’@*
alpha%>2
leaky_re_lu_132/LeakyReluØ
IdentityIdentity'leaky_re_lu_132/LeakyRelu:activations:0!^conv2d_133/Conv2D/ReadVariableOp*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2D
 conv2d_133/Conv2D/ReadVariableOp conv2d_133/Conv2D/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
Ķ
”
+__inference_conv2d_137_layer_call_fn_152175

inputs"
unknown:
	unknown_0:
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_137_layer_call_and_return_conditional_losses_1514212
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’!!: : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’!!
 
_user_specified_nameinputs
Ļ
ß
J__inference_sequential_244_layer_call_and_return_conditional_losses_151156
conv2d_134_input,
conv2d_134_151146:@0
!instance_normalization_227_151149:	0
!instance_normalization_227_151151:	
identity¢"conv2d_134/StatefulPartitionedCall¢2instance_normalization_227/StatefulPartitionedCall
"conv2d_134/StatefulPartitionedCallStatefulPartitionedCallconv2d_134_inputconv2d_134_151146*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_134_layer_call_and_return_conditional_losses_1510122$
"conv2d_134/StatefulPartitionedCall
2instance_normalization_227/StatefulPartitionedCallStatefulPartitionedCall+conv2d_134/StatefulPartitionedCall:output:0!instance_normalization_227_151149!instance_normalization_227_151151*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_15103724
2instance_normalization_227/StatefulPartitionedCallŖ
leaky_re_lu_133/PartitionedCallPartitionedCall;instance_normalization_227/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_1510482!
leaky_re_lu_133/PartitionedCallß
IdentityIdentity(leaky_re_lu_133/PartitionedCall:output:0#^conv2d_134/StatefulPartitionedCall3^instance_normalization_227/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 2H
"conv2d_134/StatefulPartitionedCall"conv2d_134/StatefulPartitionedCall2h
2instance_normalization_227/StatefulPartitionedCall2instance_normalization_227/StatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’@
*
_user_specified_nameconv2d_134_input
°
Ö
J__inference_sequential_245_layer_call_and_return_conditional_losses_151209

inputs-
conv2d_135_151171:0
!instance_normalization_228_151196:	0
!instance_normalization_228_151198:	
identity¢"conv2d_135/StatefulPartitionedCall¢2instance_normalization_228/StatefulPartitionedCall
"conv2d_135/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_135_151171*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_135_layer_call_and_return_conditional_losses_1511702$
"conv2d_135/StatefulPartitionedCall
2instance_normalization_228/StatefulPartitionedCallStatefulPartitionedCall+conv2d_135/StatefulPartitionedCall:output:0!instance_normalization_228_151196!instance_normalization_228_151198*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_15119524
2instance_normalization_228/StatefulPartitionedCallŖ
leaky_re_lu_134/PartitionedCallPartitionedCall;instance_normalization_228/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_1512062!
leaky_re_lu_134/PartitionedCallß
IdentityIdentity(leaky_re_lu_134/PartitionedCall:output:0#^conv2d_135/StatefulPartitionedCall3^instance_normalization_228/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 2H
"conv2d_135/StatefulPartitionedCall"conv2d_135/StatefulPartitionedCall2h
2instance_normalization_228/StatefulPartitionedCall2instance_normalization_228/StatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs

h
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_151321

inputs
identity
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddings
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’2
Pad
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’:r n
J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
 
_user_specified_nameinputs
×'
½
J__inference_sequential_245_layer_call_and_return_conditional_losses_152112

inputsE
)conv2d_135_conv2d_readvariableop_resource:A
2instance_normalization_228_readvariableop_resource:	G
8instance_normalization_228_add_1_readvariableop_resource:	
identity¢ conv2d_135/Conv2D/ReadVariableOp¢)instance_normalization_228/ReadVariableOp¢/instance_normalization_228/add_1/ReadVariableOpø
 conv2d_135/Conv2D/ReadVariableOpReadVariableOp)conv2d_135_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype02"
 conv2d_135/Conv2D/ReadVariableOpÅ
conv2d_135/Conv2DConv2Dinputs(conv2d_135/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2
conv2d_135/Conv2DĒ
9instance_normalization_228/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2;
9instance_normalization_228/moments/mean/reduction_indices
'instance_normalization_228/moments/meanMeanconv2d_135/Conv2D:output:0Binstance_normalization_228/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2)
'instance_normalization_228/moments/meanß
/instance_normalization_228/moments/StopGradientStopGradient0instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’21
/instance_normalization_228/moments/StopGradient
4instance_normalization_228/moments/SquaredDifferenceSquaredDifferenceconv2d_135/Conv2D:output:08instance_normalization_228/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  26
4instance_normalization_228/moments/SquaredDifferenceĻ
=instance_normalization_228/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2?
=instance_normalization_228/moments/variance/reduction_indices°
+instance_normalization_228/moments/varianceMean8instance_normalization_228/moments/SquaredDifference:z:0Finstance_normalization_228/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2-
+instance_normalization_228/moments/variance
 instance_normalization_228/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72"
 instance_normalization_228/add/yå
instance_normalization_228/addAddV24instance_normalization_228/moments/variance:output:0)instance_normalization_228/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_228/add¬
 instance_normalization_228/RsqrtRsqrt"instance_normalization_228/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_228/RsqrtŠ
instance_normalization_228/subSubconv2d_135/Conv2D:output:00instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2 
instance_normalization_228/subĢ
instance_normalization_228/mulMul"instance_normalization_228/sub:z:0$instance_normalization_228/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  2 
instance_normalization_228/mulĘ
)instance_normalization_228/ReadVariableOpReadVariableOp2instance_normalization_228_readvariableop_resource*
_output_shapes	
:*
dtype02+
)instance_normalization_228/ReadVariableOpŻ
 instance_normalization_228/mul_1Mul1instance_normalization_228/ReadVariableOp:value:0"instance_normalization_228/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  2"
 instance_normalization_228/mul_1Ų
/instance_normalization_228/add_1/ReadVariableOpReadVariableOp8instance_normalization_228_add_1_readvariableop_resource*
_output_shapes	
:*
dtype021
/instance_normalization_228/add_1/ReadVariableOpē
 instance_normalization_228/add_1AddV2$instance_normalization_228/mul_1:z:07instance_normalization_228/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  2"
 instance_normalization_228/add_1«
leaky_re_lu_134/LeakyRelu	LeakyRelu$instance_normalization_228/add_1:z:0*0
_output_shapes
:’’’’’’’’’  *
alpha%>2
leaky_re_lu_134/LeakyRelu
IdentityIdentity'leaky_re_lu_134/LeakyRelu:activations:0!^conv2d_135/Conv2D/ReadVariableOp*^instance_normalization_228/ReadVariableOp0^instance_normalization_228/add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 2D
 conv2d_135/Conv2D/ReadVariableOp conv2d_135/Conv2D/ReadVariableOp2V
)instance_normalization_228/ReadVariableOp)instance_normalization_228/ReadVariableOp2b
/instance_normalization_228/add_1/ReadVariableOp/instance_normalization_228/add_1/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs

¹
F__inference_conv2d_135_layer_call_and_return_conditional_losses_151170

inputs:
conv2d_readvariableop_resource:
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*(
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp¤
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:’’’’’’’’’@@: 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
ķ
ß
D__inference_model_17_layer_call_and_return_conditional_losses_151857

inputsR
8sequential_243_conv2d_133_conv2d_readvariableop_resource:@S
8sequential_244_conv2d_134_conv2d_readvariableop_resource:@P
Asequential_244_instance_normalization_227_readvariableop_resource:	V
Gsequential_244_instance_normalization_227_add_1_readvariableop_resource:	T
8sequential_245_conv2d_135_conv2d_readvariableop_resource:P
Asequential_245_instance_normalization_228_readvariableop_resource:	V
Gsequential_245_instance_normalization_228_add_1_readvariableop_resource:	E
)conv2d_136_conv2d_readvariableop_resource:A
2instance_normalization_229_readvariableop_resource:	G
8instance_normalization_229_add_1_readvariableop_resource:	D
)conv2d_137_conv2d_readvariableop_resource:8
*conv2d_137_biasadd_readvariableop_resource:
identity¢ conv2d_136/Conv2D/ReadVariableOp¢!conv2d_137/BiasAdd/ReadVariableOp¢ conv2d_137/Conv2D/ReadVariableOp¢)instance_normalization_229/ReadVariableOp¢/instance_normalization_229/add_1/ReadVariableOp¢/sequential_243/conv2d_133/Conv2D/ReadVariableOp¢/sequential_244/conv2d_134/Conv2D/ReadVariableOp¢8sequential_244/instance_normalization_227/ReadVariableOp¢>sequential_244/instance_normalization_227/add_1/ReadVariableOp¢/sequential_245/conv2d_135/Conv2D/ReadVariableOp¢8sequential_245/instance_normalization_228/ReadVariableOp¢>sequential_245/instance_normalization_228/add_1/ReadVariableOpć
/sequential_243/conv2d_133/Conv2D/ReadVariableOpReadVariableOp8sequential_243_conv2d_133_conv2d_readvariableop_resource*&
_output_shapes
:@*
dtype021
/sequential_243/conv2d_133/Conv2D/ReadVariableOpó
 sequential_243/conv2d_133/Conv2DConv2Dinputs7sequential_243/conv2d_133/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2"
 sequential_243/conv2d_133/Conv2DĻ
(sequential_243/leaky_re_lu_132/LeakyRelu	LeakyRelu)sequential_243/conv2d_133/Conv2D:output:0*1
_output_shapes
:’’’’’’’’’@*
alpha%>2*
(sequential_243/leaky_re_lu_132/LeakyReluä
/sequential_244/conv2d_134/Conv2D/ReadVariableOpReadVariableOp8sequential_244_conv2d_134_conv2d_readvariableop_resource*'
_output_shapes
:@*
dtype021
/sequential_244/conv2d_134/Conv2D/ReadVariableOp¢
 sequential_244/conv2d_134/Conv2DConv2D6sequential_243/leaky_re_lu_132/LeakyRelu:activations:07sequential_244/conv2d_134/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2"
 sequential_244/conv2d_134/Conv2Då
Hsequential_244/instance_normalization_227/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2J
Hsequential_244/instance_normalization_227/moments/mean/reduction_indicesĀ
6sequential_244/instance_normalization_227/moments/meanMean)sequential_244/conv2d_134/Conv2D:output:0Qsequential_244/instance_normalization_227/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(28
6sequential_244/instance_normalization_227/moments/mean
>sequential_244/instance_normalization_227/moments/StopGradientStopGradient?sequential_244/instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2@
>sequential_244/instance_normalization_227/moments/StopGradientĪ
Csequential_244/instance_normalization_227/moments/SquaredDifferenceSquaredDifference)sequential_244/conv2d_134/Conv2D:output:0Gsequential_244/instance_normalization_227/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2E
Csequential_244/instance_normalization_227/moments/SquaredDifferenceķ
Lsequential_244/instance_normalization_227/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2N
Lsequential_244/instance_normalization_227/moments/variance/reduction_indicesģ
:sequential_244/instance_normalization_227/moments/varianceMeanGsequential_244/instance_normalization_227/moments/SquaredDifference:z:0Usequential_244/instance_normalization_227/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2<
:sequential_244/instance_normalization_227/moments/variance§
/sequential_244/instance_normalization_227/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'721
/sequential_244/instance_normalization_227/add/y”
-sequential_244/instance_normalization_227/addAddV2Csequential_244/instance_normalization_227/moments/variance:output:08sequential_244/instance_normalization_227/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2/
-sequential_244/instance_normalization_227/addŁ
/sequential_244/instance_normalization_227/RsqrtRsqrt1sequential_244/instance_normalization_227/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’21
/sequential_244/instance_normalization_227/Rsqrt
-sequential_244/instance_normalization_227/subSub)sequential_244/conv2d_134/Conv2D:output:0?sequential_244/instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2/
-sequential_244/instance_normalization_227/sub
-sequential_244/instance_normalization_227/mulMul1sequential_244/instance_normalization_227/sub:z:03sequential_244/instance_normalization_227/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@2/
-sequential_244/instance_normalization_227/muló
8sequential_244/instance_normalization_227/ReadVariableOpReadVariableOpAsequential_244_instance_normalization_227_readvariableop_resource*
_output_shapes	
:*
dtype02:
8sequential_244/instance_normalization_227/ReadVariableOp
/sequential_244/instance_normalization_227/mul_1Mul@sequential_244/instance_normalization_227/ReadVariableOp:value:01sequential_244/instance_normalization_227/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@21
/sequential_244/instance_normalization_227/mul_1
>sequential_244/instance_normalization_227/add_1/ReadVariableOpReadVariableOpGsequential_244_instance_normalization_227_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02@
>sequential_244/instance_normalization_227/add_1/ReadVariableOp£
/sequential_244/instance_normalization_227/add_1AddV23sequential_244/instance_normalization_227/mul_1:z:0Fsequential_244/instance_normalization_227/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@21
/sequential_244/instance_normalization_227/add_1Ų
(sequential_244/leaky_re_lu_133/LeakyRelu	LeakyRelu3sequential_244/instance_normalization_227/add_1:z:0*0
_output_shapes
:’’’’’’’’’@@*
alpha%>2*
(sequential_244/leaky_re_lu_133/LeakyReluå
/sequential_245/conv2d_135/Conv2D/ReadVariableOpReadVariableOp8sequential_245_conv2d_135_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype021
/sequential_245/conv2d_135/Conv2D/ReadVariableOp¢
 sequential_245/conv2d_135/Conv2DConv2D6sequential_244/leaky_re_lu_133/LeakyRelu:activations:07sequential_245/conv2d_135/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2"
 sequential_245/conv2d_135/Conv2Då
Hsequential_245/instance_normalization_228/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2J
Hsequential_245/instance_normalization_228/moments/mean/reduction_indicesĀ
6sequential_245/instance_normalization_228/moments/meanMean)sequential_245/conv2d_135/Conv2D:output:0Qsequential_245/instance_normalization_228/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(28
6sequential_245/instance_normalization_228/moments/mean
>sequential_245/instance_normalization_228/moments/StopGradientStopGradient?sequential_245/instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2@
>sequential_245/instance_normalization_228/moments/StopGradientĪ
Csequential_245/instance_normalization_228/moments/SquaredDifferenceSquaredDifference)sequential_245/conv2d_135/Conv2D:output:0Gsequential_245/instance_normalization_228/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2E
Csequential_245/instance_normalization_228/moments/SquaredDifferenceķ
Lsequential_245/instance_normalization_228/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2N
Lsequential_245/instance_normalization_228/moments/variance/reduction_indicesģ
:sequential_245/instance_normalization_228/moments/varianceMeanGsequential_245/instance_normalization_228/moments/SquaredDifference:z:0Usequential_245/instance_normalization_228/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2<
:sequential_245/instance_normalization_228/moments/variance§
/sequential_245/instance_normalization_228/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'721
/sequential_245/instance_normalization_228/add/y”
-sequential_245/instance_normalization_228/addAddV2Csequential_245/instance_normalization_228/moments/variance:output:08sequential_245/instance_normalization_228/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2/
-sequential_245/instance_normalization_228/addŁ
/sequential_245/instance_normalization_228/RsqrtRsqrt1sequential_245/instance_normalization_228/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’21
/sequential_245/instance_normalization_228/Rsqrt
-sequential_245/instance_normalization_228/subSub)sequential_245/conv2d_135/Conv2D:output:0?sequential_245/instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2/
-sequential_245/instance_normalization_228/sub
-sequential_245/instance_normalization_228/mulMul1sequential_245/instance_normalization_228/sub:z:03sequential_245/instance_normalization_228/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  2/
-sequential_245/instance_normalization_228/muló
8sequential_245/instance_normalization_228/ReadVariableOpReadVariableOpAsequential_245_instance_normalization_228_readvariableop_resource*
_output_shapes	
:*
dtype02:
8sequential_245/instance_normalization_228/ReadVariableOp
/sequential_245/instance_normalization_228/mul_1Mul@sequential_245/instance_normalization_228/ReadVariableOp:value:01sequential_245/instance_normalization_228/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  21
/sequential_245/instance_normalization_228/mul_1
>sequential_245/instance_normalization_228/add_1/ReadVariableOpReadVariableOpGsequential_245_instance_normalization_228_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02@
>sequential_245/instance_normalization_228/add_1/ReadVariableOp£
/sequential_245/instance_normalization_228/add_1AddV23sequential_245/instance_normalization_228/mul_1:z:0Fsequential_245/instance_normalization_228/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  21
/sequential_245/instance_normalization_228/add_1Ų
(sequential_245/leaky_re_lu_134/LeakyRelu	LeakyRelu3sequential_245/instance_normalization_228/add_1:z:0*0
_output_shapes
:’’’’’’’’’  *
alpha%>2*
(sequential_245/leaky_re_lu_134/LeakyReluÆ
zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_2/Pad/paddingsĪ
zero_padding2d_2/PadPad6sequential_245/leaky_re_lu_134/LeakyRelu:activations:0&zero_padding2d_2/Pad/paddings:output:0*
T0*0
_output_shapes
:’’’’’’’’’""2
zero_padding2d_2/Padø
 conv2d_136/Conv2D/ReadVariableOpReadVariableOp)conv2d_136_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype02"
 conv2d_136/Conv2D/ReadVariableOpŻ
conv2d_136/Conv2DConv2Dzero_padding2d_2/Pad:output:0(conv2d_136/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
conv2d_136/Conv2DĒ
9instance_normalization_229/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2;
9instance_normalization_229/moments/mean/reduction_indices
'instance_normalization_229/moments/meanMeanconv2d_136/Conv2D:output:0Binstance_normalization_229/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2)
'instance_normalization_229/moments/meanß
/instance_normalization_229/moments/StopGradientStopGradient0instance_normalization_229/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’21
/instance_normalization_229/moments/StopGradient
4instance_normalization_229/moments/SquaredDifferenceSquaredDifferenceconv2d_136/Conv2D:output:08instance_normalization_229/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’26
4instance_normalization_229/moments/SquaredDifferenceĻ
=instance_normalization_229/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2?
=instance_normalization_229/moments/variance/reduction_indices°
+instance_normalization_229/moments/varianceMean8instance_normalization_229/moments/SquaredDifference:z:0Finstance_normalization_229/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2-
+instance_normalization_229/moments/variance
 instance_normalization_229/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72"
 instance_normalization_229/add/yå
instance_normalization_229/addAddV24instance_normalization_229/moments/variance:output:0)instance_normalization_229/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_229/add¬
 instance_normalization_229/RsqrtRsqrt"instance_normalization_229/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_229/RsqrtŠ
instance_normalization_229/subSubconv2d_136/Conv2D:output:00instance_normalization_229/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_229/subĢ
instance_normalization_229/mulMul"instance_normalization_229/sub:z:0$instance_normalization_229/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_229/mulĘ
)instance_normalization_229/ReadVariableOpReadVariableOp2instance_normalization_229_readvariableop_resource*
_output_shapes	
:*
dtype02+
)instance_normalization_229/ReadVariableOpŻ
 instance_normalization_229/mul_1Mul1instance_normalization_229/ReadVariableOp:value:0"instance_normalization_229/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_229/mul_1Ų
/instance_normalization_229/add_1/ReadVariableOpReadVariableOp8instance_normalization_229_add_1_readvariableop_resource*
_output_shapes	
:*
dtype021
/instance_normalization_229/add_1/ReadVariableOpē
 instance_normalization_229/add_1AddV2$instance_normalization_229/mul_1:z:07instance_normalization_229/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_229/add_1«
leaky_re_lu_135/LeakyRelu	LeakyRelu$instance_normalization_229/add_1:z:0*0
_output_shapes
:’’’’’’’’’*
alpha%>2
leaky_re_lu_135/LeakyReluÆ
zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_3/Pad/paddingsæ
zero_padding2d_3/PadPad'leaky_re_lu_135/LeakyRelu:activations:0&zero_padding2d_3/Pad/paddings:output:0*
T0*0
_output_shapes
:’’’’’’’’’!!2
zero_padding2d_3/Pad·
 conv2d_137/Conv2D/ReadVariableOpReadVariableOp)conv2d_137_conv2d_readvariableop_resource*'
_output_shapes
:*
dtype02"
 conv2d_137/Conv2D/ReadVariableOpÜ
conv2d_137/Conv2DConv2Dzero_padding2d_3/Pad:output:0(conv2d_137/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
conv2d_137/Conv2D­
!conv2d_137/BiasAdd/ReadVariableOpReadVariableOp*conv2d_137_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02#
!conv2d_137/BiasAdd/ReadVariableOp“
conv2d_137/BiasAddBiasAddconv2d_137/Conv2D:output:0)conv2d_137/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’2
conv2d_137/BiasAddĶ
IdentityIdentityconv2d_137/BiasAdd:output:0!^conv2d_136/Conv2D/ReadVariableOp"^conv2d_137/BiasAdd/ReadVariableOp!^conv2d_137/Conv2D/ReadVariableOp*^instance_normalization_229/ReadVariableOp0^instance_normalization_229/add_1/ReadVariableOp0^sequential_243/conv2d_133/Conv2D/ReadVariableOp0^sequential_244/conv2d_134/Conv2D/ReadVariableOp9^sequential_244/instance_normalization_227/ReadVariableOp?^sequential_244/instance_normalization_227/add_1/ReadVariableOp0^sequential_245/conv2d_135/Conv2D/ReadVariableOp9^sequential_245/instance_normalization_228/ReadVariableOp?^sequential_245/instance_normalization_228/add_1/ReadVariableOp*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2D
 conv2d_136/Conv2D/ReadVariableOp conv2d_136/Conv2D/ReadVariableOp2F
!conv2d_137/BiasAdd/ReadVariableOp!conv2d_137/BiasAdd/ReadVariableOp2D
 conv2d_137/Conv2D/ReadVariableOp conv2d_137/Conv2D/ReadVariableOp2V
)instance_normalization_229/ReadVariableOp)instance_normalization_229/ReadVariableOp2b
/instance_normalization_229/add_1/ReadVariableOp/instance_normalization_229/add_1/ReadVariableOp2b
/sequential_243/conv2d_133/Conv2D/ReadVariableOp/sequential_243/conv2d_133/Conv2D/ReadVariableOp2b
/sequential_244/conv2d_134/Conv2D/ReadVariableOp/sequential_244/conv2d_134/Conv2D/ReadVariableOp2t
8sequential_244/instance_normalization_227/ReadVariableOp8sequential_244/instance_normalization_227/ReadVariableOp2
>sequential_244/instance_normalization_227/add_1/ReadVariableOp>sequential_244/instance_normalization_227/add_1/ReadVariableOp2b
/sequential_245/conv2d_135/Conv2D/ReadVariableOp/sequential_245/conv2d_135/Conv2D/ReadVariableOp2t
8sequential_245/instance_normalization_228/ReadVariableOp8sequential_245/instance_normalization_228/ReadVariableOp2
>sequential_245/instance_normalization_228/add_1/ReadVariableOp>sequential_245/instance_normalization_228/add_1/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
Š

/__inference_sequential_243_layer_call_fn_150939
conv2d_133_input!
unknown:@
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallconv2d_133_inputunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509342
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 22
StatefulPartitionedCallStatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’
*
_user_specified_nameconv2d_133_input
·


F__inference_conv2d_137_layer_call_and_return_conditional_losses_151421

inputs9
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity¢BiasAdd/ReadVariableOp¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp¤
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
Conv2D
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’!!: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’!!
 
_user_specified_nameinputs
Ļ
ß
J__inference_sequential_244_layer_call_and_return_conditional_losses_151143
conv2d_134_input,
conv2d_134_151133:@0
!instance_normalization_227_151136:	0
!instance_normalization_227_151138:	
identity¢"conv2d_134/StatefulPartitionedCall¢2instance_normalization_227/StatefulPartitionedCall
"conv2d_134/StatefulPartitionedCallStatefulPartitionedCallconv2d_134_inputconv2d_134_151133*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_134_layer_call_and_return_conditional_losses_1510122$
"conv2d_134/StatefulPartitionedCall
2instance_normalization_227/StatefulPartitionedCallStatefulPartitionedCall+conv2d_134/StatefulPartitionedCall:output:0!instance_normalization_227_151136!instance_normalization_227_151138*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_15103724
2instance_normalization_227/StatefulPartitionedCallŖ
leaky_re_lu_133/PartitionedCallPartitionedCall;instance_normalization_227/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_1510482!
leaky_re_lu_133/PartitionedCallß
IdentityIdentity(leaky_re_lu_133/PartitionedCall:output:0#^conv2d_134/StatefulPartitionedCall3^instance_normalization_227/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 2H
"conv2d_134/StatefulPartitionedCall"conv2d_134/StatefulPartitionedCall2h
2instance_normalization_227/StatefulPartitionedCall2instance_normalization_227/StatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’@
*
_user_specified_nameconv2d_134_input

Ą
/__inference_sequential_244_layer_call_fn_151990

inputs"
unknown:@
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1511102
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
Š

/__inference_sequential_243_layer_call_fn_150982
conv2d_133_input!
unknown:@
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallconv2d_133_inputunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509702
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 22
StatefulPartitionedCallStatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’
*
_user_specified_nameconv2d_133_input

¹
F__inference_conv2d_136_layer_call_and_return_conditional_losses_151372

inputs:
conv2d_readvariableop_resource:
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*(
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp„
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:’’’’’’’’’"": 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’""
 
_user_specified_nameinputs
·


F__inference_conv2d_137_layer_call_and_return_conditional_losses_152185

inputs9
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity¢BiasAdd/ReadVariableOp¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp¤
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
Conv2D
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’!!: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’!!
 
_user_specified_nameinputs
Ŗ

+__inference_conv2d_133_layer_call_fn_152192

inputs!
unknown:@
identity¢StatefulPartitionedCallö
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_133_layer_call_and_return_conditional_losses_1509222
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
Ó&

__inference__traced_save_152376
file_prefix0
,savev2_conv2d_136_kernel_read_readvariableop?
;savev2_instance_normalization_229_scale_read_readvariableop@
<savev2_instance_normalization_229_offset_read_readvariableop0
,savev2_conv2d_137_kernel_read_readvariableop.
*savev2_conv2d_137_bias_read_readvariableop0
,savev2_conv2d_133_kernel_read_readvariableop0
,savev2_conv2d_134_kernel_read_readvariableop?
;savev2_instance_normalization_227_scale_read_readvariableop@
<savev2_instance_normalization_227_offset_read_readvariableop0
,savev2_conv2d_135_kernel_read_readvariableop?
;savev2_instance_normalization_228_scale_read_readvariableop@
<savev2_instance_normalization_228_offset_read_readvariableop
savev2_const

identity_1¢MergeV2Checkpoints
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard¦
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameĪ
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*ą
valueÖBÓB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/scale/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/offset/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB&variables/5/.ATTRIBUTES/VARIABLE_VALUEB&variables/6/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names¢
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*-
value$B"B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesÉ
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0,savev2_conv2d_136_kernel_read_readvariableop;savev2_instance_normalization_229_scale_read_readvariableop<savev2_instance_normalization_229_offset_read_readvariableop,savev2_conv2d_137_kernel_read_readvariableop*savev2_conv2d_137_bias_read_readvariableop,savev2_conv2d_133_kernel_read_readvariableop,savev2_conv2d_134_kernel_read_readvariableop;savev2_instance_normalization_227_scale_read_readvariableop<savev2_instance_normalization_227_offset_read_readvariableop,savev2_conv2d_135_kernel_read_readvariableop;savev2_instance_normalization_228_scale_read_readvariableop<savev2_instance_normalization_228_offset_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
22
SaveV2ŗ
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes”
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*©
_input_shapes
: ::::::@:@:::::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:.*
(
_output_shapes
::!

_output_shapes	
::!

_output_shapes	
::-)
'
_output_shapes
:: 

_output_shapes
::,(
&
_output_shapes
:@:-)
'
_output_shapes
:@:!

_output_shapes	
::!	

_output_shapes	
::.
*
(
_output_shapes
::!

_output_shapes	
::!

_output_shapes	
::

_output_shapes
: 
Ü
M
1__inference_zero_padding2d_3_layer_call_fn_151340

inputs
identityš
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_1513342
PartitionedCall
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’:r n
J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
 
_user_specified_nameinputs

ø
F__inference_conv2d_134_layer_call_and_return_conditional_losses_151012

inputs9
conv2d_readvariableop_resource:@
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
:@*
dtype02
Conv2D/ReadVariableOp¤
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’@: 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
®0
ņ
D__inference_model_17_layer_call_and_return_conditional_losses_151687
input_image/
sequential_243_151653:@0
sequential_244_151656:@$
sequential_244_151658:	$
sequential_244_151660:	1
sequential_245_151663:$
sequential_245_151665:	$
sequential_245_151667:	-
conv2d_136_151671:0
!instance_normalization_229_151674:	0
!instance_normalization_229_151676:	,
conv2d_137_151681:
conv2d_137_151683:
identity¢"conv2d_136/StatefulPartitionedCall¢"conv2d_137/StatefulPartitionedCall¢2instance_normalization_229/StatefulPartitionedCall¢&sequential_243/StatefulPartitionedCall¢&sequential_244/StatefulPartitionedCall¢&sequential_245/StatefulPartitionedCall«
&sequential_243/StatefulPartitionedCallStatefulPartitionedCallinput_imagesequential_243_151653*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509702(
&sequential_243/StatefulPartitionedCall
&sequential_244/StatefulPartitionedCallStatefulPartitionedCall/sequential_243/StatefulPartitionedCall:output:0sequential_244_151656sequential_244_151658sequential_244_151660*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1511102(
&sequential_244/StatefulPartitionedCall
&sequential_245/StatefulPartitionedCallStatefulPartitionedCall/sequential_244/StatefulPartitionedCall:output:0sequential_245_151663sequential_245_151665sequential_245_151667*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512682(
&sequential_245/StatefulPartitionedCall”
 zero_padding2d_2/PartitionedCallPartitionedCall/sequential_245/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’""* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_1513212"
 zero_padding2d_2/PartitionedCallø
"conv2d_136/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_2/PartitionedCall:output:0conv2d_136_151671*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_136_layer_call_and_return_conditional_losses_1513722$
"conv2d_136/StatefulPartitionedCall
2instance_normalization_229/StatefulPartitionedCallStatefulPartitionedCall+conv2d_136/StatefulPartitionedCall:output:0!instance_normalization_229_151674!instance_normalization_229_151676*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_15139724
2instance_normalization_229/StatefulPartitionedCallŖ
leaky_re_lu_135/PartitionedCallPartitionedCall;instance_normalization_229/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_1514082!
leaky_re_lu_135/PartitionedCall
 zero_padding2d_3/PartitionedCallPartitionedCall(leaky_re_lu_135/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’!!* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_1513342"
 zero_padding2d_3/PartitionedCallĢ
"conv2d_137/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_3/PartitionedCall:output:0conv2d_137_151681conv2d_137_151683*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_137_layer_call_and_return_conditional_losses_1514212$
"conv2d_137/StatefulPartitionedCall
IdentityIdentity+conv2d_137/StatefulPartitionedCall:output:0#^conv2d_136/StatefulPartitionedCall#^conv2d_137/StatefulPartitionedCall3^instance_normalization_229/StatefulPartitionedCall'^sequential_243/StatefulPartitionedCall'^sequential_244/StatefulPartitionedCall'^sequential_245/StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2H
"conv2d_136/StatefulPartitionedCall"conv2d_136/StatefulPartitionedCall2H
"conv2d_137/StatefulPartitionedCall"conv2d_137/StatefulPartitionedCall2h
2instance_normalization_229/StatefulPartitionedCall2instance_normalization_229/StatefulPartitionedCall2P
&sequential_243/StatefulPartitionedCall&sequential_243/StatefulPartitionedCall2P
&sequential_244/StatefulPartitionedCall&sequential_244/StatefulPartitionedCall2P
&sequential_245/StatefulPartitionedCall&sequential_245/StatefulPartitionedCall:^ Z
1
_output_shapes
:’’’’’’’’’
%
_user_specified_nameinput_image
”
Ź
/__inference_sequential_244_layer_call_fn_151060
conv2d_134_input"
unknown:@
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallconv2d_134_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1510512
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’@
*
_user_specified_nameconv2d_134_input

Ą
/__inference_sequential_244_layer_call_fn_151979

inputs"
unknown:@
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1510512
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
¢
g
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_152263

inputs
identitym
	LeakyRelu	LeakyReluinputs*0
_output_shapes
:’’’’’’’’’@@*
alpha%>2
	LeakyRelut
IdentityIdentityLeakyRelu:activations:0*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’@@:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
¢
g
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_151048

inputs
identitym
	LeakyRelu	LeakyReluinputs*0
_output_shapes
:’’’’’’’’’@@*
alpha%>2
	LeakyRelut
IdentityIdentityLeakyRelu:activations:0*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’@@:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
°
Ö
J__inference_sequential_245_layer_call_and_return_conditional_losses_151268

inputs-
conv2d_135_151258:0
!instance_normalization_228_151261:	0
!instance_normalization_228_151263:	
identity¢"conv2d_135/StatefulPartitionedCall¢2instance_normalization_228/StatefulPartitionedCall
"conv2d_135/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_135_151258*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_135_layer_call_and_return_conditional_losses_1511702$
"conv2d_135/StatefulPartitionedCall
2instance_normalization_228/StatefulPartitionedCallStatefulPartitionedCall+conv2d_135/StatefulPartitionedCall:output:0!instance_normalization_228_151261!instance_normalization_228_151263*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_15119524
2instance_normalization_228/StatefulPartitionedCallŖ
leaky_re_lu_134/PartitionedCallPartitionedCall;instance_normalization_228/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_1512062!
leaky_re_lu_134/PartitionedCallß
IdentityIdentity(leaky_re_lu_134/PartitionedCall:output:0#^conv2d_135/StatefulPartitionedCall3^instance_normalization_228/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 2H
"conv2d_135/StatefulPartitionedCall"conv2d_135/StatefulPartitionedCall2h
2instance_normalization_228/StatefulPartitionedCall2instance_normalization_228/StatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
ß
ī
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_152253
x&
readvariableop_resource:	,
add_1_readvariableop_resource:	
identity¢ReadVariableOp¢add_1/ReadVariableOp
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2 
moments/mean/reduction_indices
moments/meanMeanx'moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/mean
moments/StopGradientStopGradientmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/StopGradientØ
moments/SquaredDifferenceSquaredDifferencexmoments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
moments/SquaredDifference
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2$
"moments/variance/reduction_indicesÄ
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/varianceS
add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72
add/yy
addAddV2moments/variance:output:0add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
add[
RsqrtRsqrtadd:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
Rsqrtf
subSubxmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
sub`
mulMulsub:z:0	Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
mulu
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:*
dtype02
ReadVariableOpq
mul_1MulReadVariableOp:value:0mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
mul_1
add_1/ReadVariableOpReadVariableOpadd_1_readvariableop_resource*
_output_shapes	
:*
dtype02
add_1/ReadVariableOp{
add_1AddV2	mul_1:z:0add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
add_1
IdentityIdentity	add_1:z:0^ReadVariableOp^add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’@@: : 2 
ReadVariableOpReadVariableOp2,
add_1/ReadVariableOpadd_1/ReadVariableOp:S O
0
_output_shapes
:’’’’’’’’’@@

_user_specified_namex
×'
¼
J__inference_sequential_244_layer_call_and_return_conditional_losses_152040

inputsD
)conv2d_134_conv2d_readvariableop_resource:@A
2instance_normalization_227_readvariableop_resource:	G
8instance_normalization_227_add_1_readvariableop_resource:	
identity¢ conv2d_134/Conv2D/ReadVariableOp¢)instance_normalization_227/ReadVariableOp¢/instance_normalization_227/add_1/ReadVariableOp·
 conv2d_134/Conv2D/ReadVariableOpReadVariableOp)conv2d_134_conv2d_readvariableop_resource*'
_output_shapes
:@*
dtype02"
 conv2d_134/Conv2D/ReadVariableOpÅ
conv2d_134/Conv2DConv2Dinputs(conv2d_134/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2
conv2d_134/Conv2DĒ
9instance_normalization_227/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2;
9instance_normalization_227/moments/mean/reduction_indices
'instance_normalization_227/moments/meanMeanconv2d_134/Conv2D:output:0Binstance_normalization_227/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2)
'instance_normalization_227/moments/meanß
/instance_normalization_227/moments/StopGradientStopGradient0instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’21
/instance_normalization_227/moments/StopGradient
4instance_normalization_227/moments/SquaredDifferenceSquaredDifferenceconv2d_134/Conv2D:output:08instance_normalization_227/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@26
4instance_normalization_227/moments/SquaredDifferenceĻ
=instance_normalization_227/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2?
=instance_normalization_227/moments/variance/reduction_indices°
+instance_normalization_227/moments/varianceMean8instance_normalization_227/moments/SquaredDifference:z:0Finstance_normalization_227/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2-
+instance_normalization_227/moments/variance
 instance_normalization_227/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72"
 instance_normalization_227/add/yå
instance_normalization_227/addAddV24instance_normalization_227/moments/variance:output:0)instance_normalization_227/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_227/add¬
 instance_normalization_227/RsqrtRsqrt"instance_normalization_227/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_227/RsqrtŠ
instance_normalization_227/subSubconv2d_134/Conv2D:output:00instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2 
instance_normalization_227/subĢ
instance_normalization_227/mulMul"instance_normalization_227/sub:z:0$instance_normalization_227/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@2 
instance_normalization_227/mulĘ
)instance_normalization_227/ReadVariableOpReadVariableOp2instance_normalization_227_readvariableop_resource*
_output_shapes	
:*
dtype02+
)instance_normalization_227/ReadVariableOpŻ
 instance_normalization_227/mul_1Mul1instance_normalization_227/ReadVariableOp:value:0"instance_normalization_227/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@2"
 instance_normalization_227/mul_1Ų
/instance_normalization_227/add_1/ReadVariableOpReadVariableOp8instance_normalization_227_add_1_readvariableop_resource*
_output_shapes	
:*
dtype021
/instance_normalization_227/add_1/ReadVariableOpē
 instance_normalization_227/add_1AddV2$instance_normalization_227/mul_1:z:07instance_normalization_227/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@2"
 instance_normalization_227/add_1«
leaky_re_lu_133/LeakyRelu	LeakyRelu$instance_normalization_227/add_1:z:0*0
_output_shapes
:’’’’’’’’’@@*
alpha%>2
leaky_re_lu_133/LeakyRelu
IdentityIdentity'leaky_re_lu_133/LeakyRelu:activations:0!^conv2d_134/Conv2D/ReadVariableOp*^instance_normalization_227/ReadVariableOp0^instance_normalization_227/add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 2D
 conv2d_134/Conv2D/ReadVariableOp conv2d_134/Conv2D/ReadVariableOp2V
)instance_normalization_227/ReadVariableOp)instance_normalization_227/ReadVariableOp2b
/instance_normalization_227/add_1/ReadVariableOp/instance_normalization_227/add_1/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs

h
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_151334

inputs
identity
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddings
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’2
Pad
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’:r n
J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
 
_user_specified_nameinputs
Å
»
J__inference_sequential_243_layer_call_and_return_conditional_losses_150934

inputs+
conv2d_133_150923:@
identity¢"conv2d_133/StatefulPartitionedCall
"conv2d_133/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_133_150923*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_133_layer_call_and_return_conditional_losses_1509222$
"conv2d_133/StatefulPartitionedCall
leaky_re_lu_132/PartitionedCallPartitionedCall+conv2d_133/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_1509312!
leaky_re_lu_132/PartitionedCall«
IdentityIdentity(leaky_re_lu_132/PartitionedCall:output:0#^conv2d_133/StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2H
"conv2d_133/StatefulPartitionedCall"conv2d_133/StatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
ę7
Ų
"__inference__traced_restore_152422
file_prefix>
"assignvariableop_conv2d_136_kernel:B
3assignvariableop_1_instance_normalization_229_scale:	C
4assignvariableop_2_instance_normalization_229_offset:	?
$assignvariableop_3_conv2d_137_kernel:0
"assignvariableop_4_conv2d_137_bias:>
$assignvariableop_5_conv2d_133_kernel:@?
$assignvariableop_6_conv2d_134_kernel:@B
3assignvariableop_7_instance_normalization_227_scale:	C
4assignvariableop_8_instance_normalization_227_offset:	@
$assignvariableop_9_conv2d_135_kernel:C
4assignvariableop_10_instance_normalization_228_scale:	D
5assignvariableop_11_instance_normalization_228_offset:	
identity_13¢AssignVariableOp¢AssignVariableOp_1¢AssignVariableOp_10¢AssignVariableOp_11¢AssignVariableOp_2¢AssignVariableOp_3¢AssignVariableOp_4¢AssignVariableOp_5¢AssignVariableOp_6¢AssignVariableOp_7¢AssignVariableOp_8¢AssignVariableOp_9Ō
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*ą
valueÖBÓB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-4/scale/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/offset/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB&variables/5/.ATTRIBUTES/VARIABLE_VALUEB&variables/6/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesØ
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*-
value$B"B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesģ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*H
_output_shapes6
4:::::::::::::*
dtypes
22
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity”
AssignVariableOpAssignVariableOp"assignvariableop_conv2d_136_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1ø
AssignVariableOp_1AssignVariableOp3assignvariableop_1_instance_normalization_229_scaleIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2¹
AssignVariableOp_2AssignVariableOp4assignvariableop_2_instance_normalization_229_offsetIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3©
AssignVariableOp_3AssignVariableOp$assignvariableop_3_conv2d_137_kernelIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4§
AssignVariableOp_4AssignVariableOp"assignvariableop_4_conv2d_137_biasIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5©
AssignVariableOp_5AssignVariableOp$assignvariableop_5_conv2d_133_kernelIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6©
AssignVariableOp_6AssignVariableOp$assignvariableop_6_conv2d_134_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7ø
AssignVariableOp_7AssignVariableOp3assignvariableop_7_instance_normalization_227_scaleIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8¹
AssignVariableOp_8AssignVariableOp4assignvariableop_8_instance_normalization_227_offsetIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9©
AssignVariableOp_9AssignVariableOp$assignvariableop_9_conv2d_135_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10¼
AssignVariableOp_10AssignVariableOp4assignvariableop_10_instance_normalization_228_scaleIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11½
AssignVariableOp_11AssignVariableOp5assignvariableop_11_instance_normalization_228_offsetIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_119
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpę
Identity_12Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_12Ł
Identity_13IdentityIdentity_12:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
T0*
_output_shapes
: 2
Identity_13"#
identity_13Identity_13:output:0*-
_input_shapes
: : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
ß
ī
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_151397
x&
readvariableop_resource:	,
add_1_readvariableop_resource:	
identity¢ReadVariableOp¢add_1/ReadVariableOp
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2 
moments/mean/reduction_indices
moments/meanMeanx'moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/mean
moments/StopGradientStopGradientmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/StopGradientØ
moments/SquaredDifferenceSquaredDifferencexmoments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/SquaredDifference
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2$
"moments/variance/reduction_indicesÄ
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/varianceS
add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72
add/yy
addAddV2moments/variance:output:0add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
add[
RsqrtRsqrtadd:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
Rsqrtf
subSubxmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
sub`
mulMulsub:z:0	Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’2
mulu
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:*
dtype02
ReadVariableOpq
mul_1MulReadVariableOp:value:0mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
mul_1
add_1/ReadVariableOpReadVariableOpadd_1_readvariableop_resource*
_output_shapes	
:*
dtype02
add_1/ReadVariableOp{
add_1AddV2	mul_1:z:0add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’2
add_1
IdentityIdentity	add_1:z:0^ReadVariableOp^add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’: : 2 
ReadVariableOpReadVariableOp2,
add_1/ReadVariableOpadd_1/ReadVariableOp:S O
0
_output_shapes
:’’’’’’’’’

_user_specified_namex
±
Õ
J__inference_sequential_244_layer_call_and_return_conditional_losses_151051

inputs,
conv2d_134_151013:@0
!instance_normalization_227_151038:	0
!instance_normalization_227_151040:	
identity¢"conv2d_134/StatefulPartitionedCall¢2instance_normalization_227/StatefulPartitionedCall
"conv2d_134/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_134_151013*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_134_layer_call_and_return_conditional_losses_1510122$
"conv2d_134/StatefulPartitionedCall
2instance_normalization_227/StatefulPartitionedCallStatefulPartitionedCall+conv2d_134/StatefulPartitionedCall:output:0!instance_normalization_227_151038!instance_normalization_227_151040*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_15103724
2instance_normalization_227/StatefulPartitionedCallŖ
leaky_re_lu_133/PartitionedCallPartitionedCall;instance_normalization_227/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_1510482!
leaky_re_lu_133/PartitionedCallß
IdentityIdentity(leaky_re_lu_133/PartitionedCall:output:0#^conv2d_134/StatefulPartitionedCall3^instance_normalization_227/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 2H
"conv2d_134/StatefulPartitionedCall"conv2d_134/StatefulPartitionedCall2h
2instance_normalization_227/StatefulPartitionedCall2instance_normalization_227/StatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
’

Õ
$__inference_signature_wrapper_151718
input_image!
unknown:@$
	unknown_0:@
	unknown_1:	
	unknown_2:	%
	unknown_3:
	unknown_4:	
	unknown_5:	%
	unknown_6:
	unknown_7:	
	unknown_8:	$
	unknown_9:

unknown_10:
identity¢StatefulPartitionedCallä
StatefulPartitionedCallStatefulPartitionedCallinput_imageunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8 **
f%R#
!__inference__wrapped_model_1509082
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:^ Z
1
_output_shapes
:’’’’’’’’’
%
_user_specified_nameinput_image
Å
»
J__inference_sequential_243_layer_call_and_return_conditional_losses_150970

inputs+
conv2d_133_150965:@
identity¢"conv2d_133/StatefulPartitionedCall
"conv2d_133/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_133_150965*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_133_layer_call_and_return_conditional_losses_1509222$
"conv2d_133/StatefulPartitionedCall
leaky_re_lu_132/PartitionedCallPartitionedCall+conv2d_133/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_1509312!
leaky_re_lu_132/PartitionedCall«
IdentityIdentity(leaky_re_lu_132/PartitionedCall:output:0#^conv2d_133/StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2H
"conv2d_133/StatefulPartitionedCall"conv2d_133/StatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
ß
ī
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_152307
x&
readvariableop_resource:	,
add_1_readvariableop_resource:	
identity¢ReadVariableOp¢add_1/ReadVariableOp
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2 
moments/mean/reduction_indices
moments/meanMeanx'moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/mean
moments/StopGradientStopGradientmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/StopGradientØ
moments/SquaredDifferenceSquaredDifferencexmoments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2
moments/SquaredDifference
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2$
"moments/variance/reduction_indicesÄ
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/varianceS
add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72
add/yy
addAddV2moments/variance:output:0add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
add[
RsqrtRsqrtadd:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
Rsqrtf
subSubxmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2
sub`
mulMulsub:z:0	Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  2
mulu
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:*
dtype02
ReadVariableOpq
mul_1MulReadVariableOp:value:0mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  2
mul_1
add_1/ReadVariableOpReadVariableOpadd_1_readvariableop_resource*
_output_shapes	
:*
dtype02
add_1/ReadVariableOp{
add_1AddV2	mul_1:z:0add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  2
add_1
IdentityIdentity	add_1:z:0^ReadVariableOp^add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’  : : 2 
ReadVariableOpReadVariableOp2,
add_1/ReadVariableOpadd_1/ReadVariableOp:S O
0
_output_shapes
:’’’’’’’’’  

_user_specified_namex
²

/__inference_sequential_243_layer_call_fn_151952

inputs!
unknown:@
identity¢StatefulPartitionedCallś
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509702
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
¬

!__inference__wrapped_model_150908
input_image[
Amodel_17_sequential_243_conv2d_133_conv2d_readvariableop_resource:@\
Amodel_17_sequential_244_conv2d_134_conv2d_readvariableop_resource:@Y
Jmodel_17_sequential_244_instance_normalization_227_readvariableop_resource:	_
Pmodel_17_sequential_244_instance_normalization_227_add_1_readvariableop_resource:	]
Amodel_17_sequential_245_conv2d_135_conv2d_readvariableop_resource:Y
Jmodel_17_sequential_245_instance_normalization_228_readvariableop_resource:	_
Pmodel_17_sequential_245_instance_normalization_228_add_1_readvariableop_resource:	N
2model_17_conv2d_136_conv2d_readvariableop_resource:J
;model_17_instance_normalization_229_readvariableop_resource:	P
Amodel_17_instance_normalization_229_add_1_readvariableop_resource:	M
2model_17_conv2d_137_conv2d_readvariableop_resource:A
3model_17_conv2d_137_biasadd_readvariableop_resource:
identity¢)model_17/conv2d_136/Conv2D/ReadVariableOp¢*model_17/conv2d_137/BiasAdd/ReadVariableOp¢)model_17/conv2d_137/Conv2D/ReadVariableOp¢2model_17/instance_normalization_229/ReadVariableOp¢8model_17/instance_normalization_229/add_1/ReadVariableOp¢8model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOp¢8model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOp¢Amodel_17/sequential_244/instance_normalization_227/ReadVariableOp¢Gmodel_17/sequential_244/instance_normalization_227/add_1/ReadVariableOp¢8model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOp¢Amodel_17/sequential_245/instance_normalization_228/ReadVariableOp¢Gmodel_17/sequential_245/instance_normalization_228/add_1/ReadVariableOpž
8model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOpReadVariableOpAmodel_17_sequential_243_conv2d_133_conv2d_readvariableop_resource*&
_output_shapes
:@*
dtype02:
8model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOp
)model_17/sequential_243/conv2d_133/Conv2DConv2Dinput_image@model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2+
)model_17/sequential_243/conv2d_133/Conv2Dź
1model_17/sequential_243/leaky_re_lu_132/LeakyRelu	LeakyRelu2model_17/sequential_243/conv2d_133/Conv2D:output:0*1
_output_shapes
:’’’’’’’’’@*
alpha%>23
1model_17/sequential_243/leaky_re_lu_132/LeakyRelu’
8model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOpReadVariableOpAmodel_17_sequential_244_conv2d_134_conv2d_readvariableop_resource*'
_output_shapes
:@*
dtype02:
8model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOpĘ
)model_17/sequential_244/conv2d_134/Conv2DConv2D?model_17/sequential_243/leaky_re_lu_132/LeakyRelu:activations:0@model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2+
)model_17/sequential_244/conv2d_134/Conv2D÷
Qmodel_17/sequential_244/instance_normalization_227/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2S
Qmodel_17/sequential_244/instance_normalization_227/moments/mean/reduction_indicesę
?model_17/sequential_244/instance_normalization_227/moments/meanMean2model_17/sequential_244/conv2d_134/Conv2D:output:0Zmodel_17/sequential_244/instance_normalization_227/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2A
?model_17/sequential_244/instance_normalization_227/moments/mean§
Gmodel_17/sequential_244/instance_normalization_227/moments/StopGradientStopGradientHmodel_17/sequential_244/instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2I
Gmodel_17/sequential_244/instance_normalization_227/moments/StopGradientņ
Lmodel_17/sequential_244/instance_normalization_227/moments/SquaredDifferenceSquaredDifference2model_17/sequential_244/conv2d_134/Conv2D:output:0Pmodel_17/sequential_244/instance_normalization_227/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2N
Lmodel_17/sequential_244/instance_normalization_227/moments/SquaredDifference’
Umodel_17/sequential_244/instance_normalization_227/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2W
Umodel_17/sequential_244/instance_normalization_227/moments/variance/reduction_indices
Cmodel_17/sequential_244/instance_normalization_227/moments/varianceMeanPmodel_17/sequential_244/instance_normalization_227/moments/SquaredDifference:z:0^model_17/sequential_244/instance_normalization_227/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2E
Cmodel_17/sequential_244/instance_normalization_227/moments/variance¹
8model_17/sequential_244/instance_normalization_227/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72:
8model_17/sequential_244/instance_normalization_227/add/yÅ
6model_17/sequential_244/instance_normalization_227/addAddV2Lmodel_17/sequential_244/instance_normalization_227/moments/variance:output:0Amodel_17/sequential_244/instance_normalization_227/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’28
6model_17/sequential_244/instance_normalization_227/addō
8model_17/sequential_244/instance_normalization_227/RsqrtRsqrt:model_17/sequential_244/instance_normalization_227/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2:
8model_17/sequential_244/instance_normalization_227/Rsqrt°
6model_17/sequential_244/instance_normalization_227/subSub2model_17/sequential_244/conv2d_134/Conv2D:output:0Hmodel_17/sequential_244/instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@28
6model_17/sequential_244/instance_normalization_227/sub¬
6model_17/sequential_244/instance_normalization_227/mulMul:model_17/sequential_244/instance_normalization_227/sub:z:0<model_17/sequential_244/instance_normalization_227/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@28
6model_17/sequential_244/instance_normalization_227/mul
Amodel_17/sequential_244/instance_normalization_227/ReadVariableOpReadVariableOpJmodel_17_sequential_244_instance_normalization_227_readvariableop_resource*
_output_shapes	
:*
dtype02C
Amodel_17/sequential_244/instance_normalization_227/ReadVariableOp½
8model_17/sequential_244/instance_normalization_227/mul_1MulImodel_17/sequential_244/instance_normalization_227/ReadVariableOp:value:0:model_17/sequential_244/instance_normalization_227/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@2:
8model_17/sequential_244/instance_normalization_227/mul_1 
Gmodel_17/sequential_244/instance_normalization_227/add_1/ReadVariableOpReadVariableOpPmodel_17_sequential_244_instance_normalization_227_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02I
Gmodel_17/sequential_244/instance_normalization_227/add_1/ReadVariableOpĒ
8model_17/sequential_244/instance_normalization_227/add_1AddV2<model_17/sequential_244/instance_normalization_227/mul_1:z:0Omodel_17/sequential_244/instance_normalization_227/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@2:
8model_17/sequential_244/instance_normalization_227/add_1ó
1model_17/sequential_244/leaky_re_lu_133/LeakyRelu	LeakyRelu<model_17/sequential_244/instance_normalization_227/add_1:z:0*0
_output_shapes
:’’’’’’’’’@@*
alpha%>23
1model_17/sequential_244/leaky_re_lu_133/LeakyRelu
8model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOpReadVariableOpAmodel_17_sequential_245_conv2d_135_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype02:
8model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOpĘ
)model_17/sequential_245/conv2d_135/Conv2DConv2D?model_17/sequential_244/leaky_re_lu_133/LeakyRelu:activations:0@model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2+
)model_17/sequential_245/conv2d_135/Conv2D÷
Qmodel_17/sequential_245/instance_normalization_228/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2S
Qmodel_17/sequential_245/instance_normalization_228/moments/mean/reduction_indicesę
?model_17/sequential_245/instance_normalization_228/moments/meanMean2model_17/sequential_245/conv2d_135/Conv2D:output:0Zmodel_17/sequential_245/instance_normalization_228/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2A
?model_17/sequential_245/instance_normalization_228/moments/mean§
Gmodel_17/sequential_245/instance_normalization_228/moments/StopGradientStopGradientHmodel_17/sequential_245/instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2I
Gmodel_17/sequential_245/instance_normalization_228/moments/StopGradientņ
Lmodel_17/sequential_245/instance_normalization_228/moments/SquaredDifferenceSquaredDifference2model_17/sequential_245/conv2d_135/Conv2D:output:0Pmodel_17/sequential_245/instance_normalization_228/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2N
Lmodel_17/sequential_245/instance_normalization_228/moments/SquaredDifference’
Umodel_17/sequential_245/instance_normalization_228/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2W
Umodel_17/sequential_245/instance_normalization_228/moments/variance/reduction_indices
Cmodel_17/sequential_245/instance_normalization_228/moments/varianceMeanPmodel_17/sequential_245/instance_normalization_228/moments/SquaredDifference:z:0^model_17/sequential_245/instance_normalization_228/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2E
Cmodel_17/sequential_245/instance_normalization_228/moments/variance¹
8model_17/sequential_245/instance_normalization_228/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72:
8model_17/sequential_245/instance_normalization_228/add/yÅ
6model_17/sequential_245/instance_normalization_228/addAddV2Lmodel_17/sequential_245/instance_normalization_228/moments/variance:output:0Amodel_17/sequential_245/instance_normalization_228/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’28
6model_17/sequential_245/instance_normalization_228/addō
8model_17/sequential_245/instance_normalization_228/RsqrtRsqrt:model_17/sequential_245/instance_normalization_228/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2:
8model_17/sequential_245/instance_normalization_228/Rsqrt°
6model_17/sequential_245/instance_normalization_228/subSub2model_17/sequential_245/conv2d_135/Conv2D:output:0Hmodel_17/sequential_245/instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  28
6model_17/sequential_245/instance_normalization_228/sub¬
6model_17/sequential_245/instance_normalization_228/mulMul:model_17/sequential_245/instance_normalization_228/sub:z:0<model_17/sequential_245/instance_normalization_228/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  28
6model_17/sequential_245/instance_normalization_228/mul
Amodel_17/sequential_245/instance_normalization_228/ReadVariableOpReadVariableOpJmodel_17_sequential_245_instance_normalization_228_readvariableop_resource*
_output_shapes	
:*
dtype02C
Amodel_17/sequential_245/instance_normalization_228/ReadVariableOp½
8model_17/sequential_245/instance_normalization_228/mul_1MulImodel_17/sequential_245/instance_normalization_228/ReadVariableOp:value:0:model_17/sequential_245/instance_normalization_228/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  2:
8model_17/sequential_245/instance_normalization_228/mul_1 
Gmodel_17/sequential_245/instance_normalization_228/add_1/ReadVariableOpReadVariableOpPmodel_17_sequential_245_instance_normalization_228_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02I
Gmodel_17/sequential_245/instance_normalization_228/add_1/ReadVariableOpĒ
8model_17/sequential_245/instance_normalization_228/add_1AddV2<model_17/sequential_245/instance_normalization_228/mul_1:z:0Omodel_17/sequential_245/instance_normalization_228/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  2:
8model_17/sequential_245/instance_normalization_228/add_1ó
1model_17/sequential_245/leaky_re_lu_134/LeakyRelu	LeakyRelu<model_17/sequential_245/instance_normalization_228/add_1:z:0*0
_output_shapes
:’’’’’’’’’  *
alpha%>23
1model_17/sequential_245/leaky_re_lu_134/LeakyReluĮ
&model_17/zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2(
&model_17/zero_padding2d_2/Pad/paddingsņ
model_17/zero_padding2d_2/PadPad?model_17/sequential_245/leaky_re_lu_134/LeakyRelu:activations:0/model_17/zero_padding2d_2/Pad/paddings:output:0*
T0*0
_output_shapes
:’’’’’’’’’""2
model_17/zero_padding2d_2/PadÓ
)model_17/conv2d_136/Conv2D/ReadVariableOpReadVariableOp2model_17_conv2d_136_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype02+
)model_17/conv2d_136/Conv2D/ReadVariableOp
model_17/conv2d_136/Conv2DConv2D&model_17/zero_padding2d_2/Pad:output:01model_17/conv2d_136/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
model_17/conv2d_136/Conv2DŁ
Bmodel_17/instance_normalization_229/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2D
Bmodel_17/instance_normalization_229/moments/mean/reduction_indicesŖ
0model_17/instance_normalization_229/moments/meanMean#model_17/conv2d_136/Conv2D:output:0Kmodel_17/instance_normalization_229/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(22
0model_17/instance_normalization_229/moments/meanś
8model_17/instance_normalization_229/moments/StopGradientStopGradient9model_17/instance_normalization_229/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2:
8model_17/instance_normalization_229/moments/StopGradient¶
=model_17/instance_normalization_229/moments/SquaredDifferenceSquaredDifference#model_17/conv2d_136/Conv2D:output:0Amodel_17/instance_normalization_229/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’2?
=model_17/instance_normalization_229/moments/SquaredDifferenceį
Fmodel_17/instance_normalization_229/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2H
Fmodel_17/instance_normalization_229/moments/variance/reduction_indicesŌ
4model_17/instance_normalization_229/moments/varianceMeanAmodel_17/instance_normalization_229/moments/SquaredDifference:z:0Omodel_17/instance_normalization_229/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(26
4model_17/instance_normalization_229/moments/variance
)model_17/instance_normalization_229/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72+
)model_17/instance_normalization_229/add/y
'model_17/instance_normalization_229/addAddV2=model_17/instance_normalization_229/moments/variance:output:02model_17/instance_normalization_229/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2)
'model_17/instance_normalization_229/addĒ
)model_17/instance_normalization_229/RsqrtRsqrt+model_17/instance_normalization_229/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2+
)model_17/instance_normalization_229/Rsqrtō
'model_17/instance_normalization_229/subSub#model_17/conv2d_136/Conv2D:output:09model_17/instance_normalization_229/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2)
'model_17/instance_normalization_229/subš
'model_17/instance_normalization_229/mulMul+model_17/instance_normalization_229/sub:z:0-model_17/instance_normalization_229/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’2)
'model_17/instance_normalization_229/mulį
2model_17/instance_normalization_229/ReadVariableOpReadVariableOp;model_17_instance_normalization_229_readvariableop_resource*
_output_shapes	
:*
dtype024
2model_17/instance_normalization_229/ReadVariableOp
)model_17/instance_normalization_229/mul_1Mul:model_17/instance_normalization_229/ReadVariableOp:value:0+model_17/instance_normalization_229/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’2+
)model_17/instance_normalization_229/mul_1ó
8model_17/instance_normalization_229/add_1/ReadVariableOpReadVariableOpAmodel_17_instance_normalization_229_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02:
8model_17/instance_normalization_229/add_1/ReadVariableOp
)model_17/instance_normalization_229/add_1AddV2-model_17/instance_normalization_229/mul_1:z:0@model_17/instance_normalization_229/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’2+
)model_17/instance_normalization_229/add_1Ę
"model_17/leaky_re_lu_135/LeakyRelu	LeakyRelu-model_17/instance_normalization_229/add_1:z:0*0
_output_shapes
:’’’’’’’’’*
alpha%>2$
"model_17/leaky_re_lu_135/LeakyReluĮ
&model_17/zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2(
&model_17/zero_padding2d_3/Pad/paddingsć
model_17/zero_padding2d_3/PadPad0model_17/leaky_re_lu_135/LeakyRelu:activations:0/model_17/zero_padding2d_3/Pad/paddings:output:0*
T0*0
_output_shapes
:’’’’’’’’’!!2
model_17/zero_padding2d_3/PadŅ
)model_17/conv2d_137/Conv2D/ReadVariableOpReadVariableOp2model_17_conv2d_137_conv2d_readvariableop_resource*'
_output_shapes
:*
dtype02+
)model_17/conv2d_137/Conv2D/ReadVariableOp
model_17/conv2d_137/Conv2DConv2D&model_17/zero_padding2d_3/Pad:output:01model_17/conv2d_137/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
model_17/conv2d_137/Conv2DČ
*model_17/conv2d_137/BiasAdd/ReadVariableOpReadVariableOp3model_17_conv2d_137_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02,
*model_17/conv2d_137/BiasAdd/ReadVariableOpŲ
model_17/conv2d_137/BiasAddBiasAdd#model_17/conv2d_137/Conv2D:output:02model_17/conv2d_137/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’2
model_17/conv2d_137/BiasAddĀ
IdentityIdentity$model_17/conv2d_137/BiasAdd:output:0*^model_17/conv2d_136/Conv2D/ReadVariableOp+^model_17/conv2d_137/BiasAdd/ReadVariableOp*^model_17/conv2d_137/Conv2D/ReadVariableOp3^model_17/instance_normalization_229/ReadVariableOp9^model_17/instance_normalization_229/add_1/ReadVariableOp9^model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOp9^model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOpB^model_17/sequential_244/instance_normalization_227/ReadVariableOpH^model_17/sequential_244/instance_normalization_227/add_1/ReadVariableOp9^model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOpB^model_17/sequential_245/instance_normalization_228/ReadVariableOpH^model_17/sequential_245/instance_normalization_228/add_1/ReadVariableOp*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2V
)model_17/conv2d_136/Conv2D/ReadVariableOp)model_17/conv2d_136/Conv2D/ReadVariableOp2X
*model_17/conv2d_137/BiasAdd/ReadVariableOp*model_17/conv2d_137/BiasAdd/ReadVariableOp2V
)model_17/conv2d_137/Conv2D/ReadVariableOp)model_17/conv2d_137/Conv2D/ReadVariableOp2h
2model_17/instance_normalization_229/ReadVariableOp2model_17/instance_normalization_229/ReadVariableOp2t
8model_17/instance_normalization_229/add_1/ReadVariableOp8model_17/instance_normalization_229/add_1/ReadVariableOp2t
8model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOp8model_17/sequential_243/conv2d_133/Conv2D/ReadVariableOp2t
8model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOp8model_17/sequential_244/conv2d_134/Conv2D/ReadVariableOp2
Amodel_17/sequential_244/instance_normalization_227/ReadVariableOpAmodel_17/sequential_244/instance_normalization_227/ReadVariableOp2
Gmodel_17/sequential_244/instance_normalization_227/add_1/ReadVariableOpGmodel_17/sequential_244/instance_normalization_227/add_1/ReadVariableOp2t
8model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOp8model_17/sequential_245/conv2d_135/Conv2D/ReadVariableOp2
Amodel_17/sequential_245/instance_normalization_228/ReadVariableOpAmodel_17/sequential_245/instance_normalization_228/ReadVariableOp2
Gmodel_17/sequential_245/instance_normalization_228/add_1/ReadVariableOpGmodel_17/sequential_245/instance_normalization_228/add_1/ReadVariableOp:^ Z
1
_output_shapes
:’’’’’’’’’
%
_user_specified_nameinput_image
®0
ņ
D__inference_model_17_layer_call_and_return_conditional_losses_151650
input_image/
sequential_243_151616:@0
sequential_244_151619:@$
sequential_244_151621:	$
sequential_244_151623:	1
sequential_245_151626:$
sequential_245_151628:	$
sequential_245_151630:	-
conv2d_136_151634:0
!instance_normalization_229_151637:	0
!instance_normalization_229_151639:	,
conv2d_137_151644:
conv2d_137_151646:
identity¢"conv2d_136/StatefulPartitionedCall¢"conv2d_137/StatefulPartitionedCall¢2instance_normalization_229/StatefulPartitionedCall¢&sequential_243/StatefulPartitionedCall¢&sequential_244/StatefulPartitionedCall¢&sequential_245/StatefulPartitionedCall«
&sequential_243/StatefulPartitionedCallStatefulPartitionedCallinput_imagesequential_243_151616*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509342(
&sequential_243/StatefulPartitionedCall
&sequential_244/StatefulPartitionedCallStatefulPartitionedCall/sequential_243/StatefulPartitionedCall:output:0sequential_244_151619sequential_244_151621sequential_244_151623*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1510512(
&sequential_244/StatefulPartitionedCall
&sequential_245/StatefulPartitionedCallStatefulPartitionedCall/sequential_244/StatefulPartitionedCall:output:0sequential_245_151626sequential_245_151628sequential_245_151630*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512092(
&sequential_245/StatefulPartitionedCall”
 zero_padding2d_2/PartitionedCallPartitionedCall/sequential_245/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’""* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_1513212"
 zero_padding2d_2/PartitionedCallø
"conv2d_136/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_2/PartitionedCall:output:0conv2d_136_151634*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_136_layer_call_and_return_conditional_losses_1513722$
"conv2d_136/StatefulPartitionedCall
2instance_normalization_229/StatefulPartitionedCallStatefulPartitionedCall+conv2d_136/StatefulPartitionedCall:output:0!instance_normalization_229_151637!instance_normalization_229_151639*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_15139724
2instance_normalization_229/StatefulPartitionedCallŖ
leaky_re_lu_135/PartitionedCallPartitionedCall;instance_normalization_229/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_1514082!
leaky_re_lu_135/PartitionedCall
 zero_padding2d_3/PartitionedCallPartitionedCall(leaky_re_lu_135/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’!!* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_1513342"
 zero_padding2d_3/PartitionedCallĢ
"conv2d_137/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_3/PartitionedCall:output:0conv2d_137_151644conv2d_137_151646*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_137_layer_call_and_return_conditional_losses_1514212$
"conv2d_137/StatefulPartitionedCall
IdentityIdentity+conv2d_137/StatefulPartitionedCall:output:0#^conv2d_136/StatefulPartitionedCall#^conv2d_137/StatefulPartitionedCall3^instance_normalization_229/StatefulPartitionedCall'^sequential_243/StatefulPartitionedCall'^sequential_244/StatefulPartitionedCall'^sequential_245/StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2H
"conv2d_136/StatefulPartitionedCall"conv2d_136/StatefulPartitionedCall2H
"conv2d_137/StatefulPartitionedCall"conv2d_137/StatefulPartitionedCall2h
2instance_normalization_229/StatefulPartitionedCall2instance_normalization_229/StatefulPartitionedCall2P
&sequential_243/StatefulPartitionedCall&sequential_243/StatefulPartitionedCall2P
&sequential_244/StatefulPartitionedCall&sequential_244/StatefulPartitionedCall2P
&sequential_245/StatefulPartitionedCall&sequential_245/StatefulPartitionedCall:^ Z
1
_output_shapes
:’’’’’’’’’
%
_user_specified_nameinput_image
 
Ė
/__inference_sequential_245_layer_call_fn_151288
conv2d_135_input#
unknown:
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallconv2d_135_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512682
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
0
_output_shapes
:’’’’’’’’’@@
*
_user_specified_nameconv2d_135_input
Ø

+__inference_conv2d_135_layer_call_fn_152270

inputs#
unknown:
identity¢StatefulPartitionedCallõ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_135_layer_call_and_return_conditional_losses_1511702
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:’’’’’’’’’@@: 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
¢
g
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_151408

inputs
identitym
	LeakyRelu	LeakyReluinputs*0
_output_shapes
:’’’’’’’’’*
alpha%>2
	LeakyRelut
IdentityIdentityLeakyRelu:activations:0*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’:X T
0
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
ń
L
0__inference_leaky_re_lu_134_layer_call_fn_152312

inputs
identityÕ
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_1512062
PartitionedCallu
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’  :X T
0
_output_shapes
:’’’’’’’’’  
 
_user_specified_nameinputs
©

+__inference_conv2d_134_layer_call_fn_152216

inputs"
unknown:@
identity¢StatefulPartitionedCallõ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_134_layer_call_and_return_conditional_losses_1510122
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’@: 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
ß
ī
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_151195
x&
readvariableop_resource:	,
add_1_readvariableop_resource:	
identity¢ReadVariableOp¢add_1/ReadVariableOp
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2 
moments/mean/reduction_indices
moments/meanMeanx'moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/mean
moments/StopGradientStopGradientmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/StopGradientØ
moments/SquaredDifferenceSquaredDifferencexmoments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2
moments/SquaredDifference
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2$
"moments/variance/reduction_indicesÄ
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/varianceS
add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72
add/yy
addAddV2moments/variance:output:0add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
add[
RsqrtRsqrtadd:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
Rsqrtf
subSubxmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2
sub`
mulMulsub:z:0	Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  2
mulu
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:*
dtype02
ReadVariableOpq
mul_1MulReadVariableOp:value:0mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  2
mul_1
add_1/ReadVariableOpReadVariableOpadd_1_readvariableop_resource*
_output_shapes	
:*
dtype02
add_1/ReadVariableOp{
add_1AddV2	mul_1:z:0add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  2
add_1
IdentityIdentity	add_1:z:0^ReadVariableOp^add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’  : : 2 
ReadVariableOpReadVariableOp2,
add_1/ReadVariableOpadd_1/ReadVariableOp:S O
0
_output_shapes
:’’’’’’’’’  

_user_specified_namex

Į
/__inference_sequential_245_layer_call_fn_152062

inputs#
unknown:
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512682
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
ķ
ß
D__inference_model_17_layer_call_and_return_conditional_losses_151938

inputsR
8sequential_243_conv2d_133_conv2d_readvariableop_resource:@S
8sequential_244_conv2d_134_conv2d_readvariableop_resource:@P
Asequential_244_instance_normalization_227_readvariableop_resource:	V
Gsequential_244_instance_normalization_227_add_1_readvariableop_resource:	T
8sequential_245_conv2d_135_conv2d_readvariableop_resource:P
Asequential_245_instance_normalization_228_readvariableop_resource:	V
Gsequential_245_instance_normalization_228_add_1_readvariableop_resource:	E
)conv2d_136_conv2d_readvariableop_resource:A
2instance_normalization_229_readvariableop_resource:	G
8instance_normalization_229_add_1_readvariableop_resource:	D
)conv2d_137_conv2d_readvariableop_resource:8
*conv2d_137_biasadd_readvariableop_resource:
identity¢ conv2d_136/Conv2D/ReadVariableOp¢!conv2d_137/BiasAdd/ReadVariableOp¢ conv2d_137/Conv2D/ReadVariableOp¢)instance_normalization_229/ReadVariableOp¢/instance_normalization_229/add_1/ReadVariableOp¢/sequential_243/conv2d_133/Conv2D/ReadVariableOp¢/sequential_244/conv2d_134/Conv2D/ReadVariableOp¢8sequential_244/instance_normalization_227/ReadVariableOp¢>sequential_244/instance_normalization_227/add_1/ReadVariableOp¢/sequential_245/conv2d_135/Conv2D/ReadVariableOp¢8sequential_245/instance_normalization_228/ReadVariableOp¢>sequential_245/instance_normalization_228/add_1/ReadVariableOpć
/sequential_243/conv2d_133/Conv2D/ReadVariableOpReadVariableOp8sequential_243_conv2d_133_conv2d_readvariableop_resource*&
_output_shapes
:@*
dtype021
/sequential_243/conv2d_133/Conv2D/ReadVariableOpó
 sequential_243/conv2d_133/Conv2DConv2Dinputs7sequential_243/conv2d_133/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2"
 sequential_243/conv2d_133/Conv2DĻ
(sequential_243/leaky_re_lu_132/LeakyRelu	LeakyRelu)sequential_243/conv2d_133/Conv2D:output:0*1
_output_shapes
:’’’’’’’’’@*
alpha%>2*
(sequential_243/leaky_re_lu_132/LeakyReluä
/sequential_244/conv2d_134/Conv2D/ReadVariableOpReadVariableOp8sequential_244_conv2d_134_conv2d_readvariableop_resource*'
_output_shapes
:@*
dtype021
/sequential_244/conv2d_134/Conv2D/ReadVariableOp¢
 sequential_244/conv2d_134/Conv2DConv2D6sequential_243/leaky_re_lu_132/LeakyRelu:activations:07sequential_244/conv2d_134/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2"
 sequential_244/conv2d_134/Conv2Då
Hsequential_244/instance_normalization_227/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2J
Hsequential_244/instance_normalization_227/moments/mean/reduction_indicesĀ
6sequential_244/instance_normalization_227/moments/meanMean)sequential_244/conv2d_134/Conv2D:output:0Qsequential_244/instance_normalization_227/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(28
6sequential_244/instance_normalization_227/moments/mean
>sequential_244/instance_normalization_227/moments/StopGradientStopGradient?sequential_244/instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2@
>sequential_244/instance_normalization_227/moments/StopGradientĪ
Csequential_244/instance_normalization_227/moments/SquaredDifferenceSquaredDifference)sequential_244/conv2d_134/Conv2D:output:0Gsequential_244/instance_normalization_227/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2E
Csequential_244/instance_normalization_227/moments/SquaredDifferenceķ
Lsequential_244/instance_normalization_227/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2N
Lsequential_244/instance_normalization_227/moments/variance/reduction_indicesģ
:sequential_244/instance_normalization_227/moments/varianceMeanGsequential_244/instance_normalization_227/moments/SquaredDifference:z:0Usequential_244/instance_normalization_227/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2<
:sequential_244/instance_normalization_227/moments/variance§
/sequential_244/instance_normalization_227/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'721
/sequential_244/instance_normalization_227/add/y”
-sequential_244/instance_normalization_227/addAddV2Csequential_244/instance_normalization_227/moments/variance:output:08sequential_244/instance_normalization_227/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2/
-sequential_244/instance_normalization_227/addŁ
/sequential_244/instance_normalization_227/RsqrtRsqrt1sequential_244/instance_normalization_227/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’21
/sequential_244/instance_normalization_227/Rsqrt
-sequential_244/instance_normalization_227/subSub)sequential_244/conv2d_134/Conv2D:output:0?sequential_244/instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2/
-sequential_244/instance_normalization_227/sub
-sequential_244/instance_normalization_227/mulMul1sequential_244/instance_normalization_227/sub:z:03sequential_244/instance_normalization_227/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@2/
-sequential_244/instance_normalization_227/muló
8sequential_244/instance_normalization_227/ReadVariableOpReadVariableOpAsequential_244_instance_normalization_227_readvariableop_resource*
_output_shapes	
:*
dtype02:
8sequential_244/instance_normalization_227/ReadVariableOp
/sequential_244/instance_normalization_227/mul_1Mul@sequential_244/instance_normalization_227/ReadVariableOp:value:01sequential_244/instance_normalization_227/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@21
/sequential_244/instance_normalization_227/mul_1
>sequential_244/instance_normalization_227/add_1/ReadVariableOpReadVariableOpGsequential_244_instance_normalization_227_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02@
>sequential_244/instance_normalization_227/add_1/ReadVariableOp£
/sequential_244/instance_normalization_227/add_1AddV23sequential_244/instance_normalization_227/mul_1:z:0Fsequential_244/instance_normalization_227/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@21
/sequential_244/instance_normalization_227/add_1Ų
(sequential_244/leaky_re_lu_133/LeakyRelu	LeakyRelu3sequential_244/instance_normalization_227/add_1:z:0*0
_output_shapes
:’’’’’’’’’@@*
alpha%>2*
(sequential_244/leaky_re_lu_133/LeakyReluå
/sequential_245/conv2d_135/Conv2D/ReadVariableOpReadVariableOp8sequential_245_conv2d_135_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype021
/sequential_245/conv2d_135/Conv2D/ReadVariableOp¢
 sequential_245/conv2d_135/Conv2DConv2D6sequential_244/leaky_re_lu_133/LeakyRelu:activations:07sequential_245/conv2d_135/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2"
 sequential_245/conv2d_135/Conv2Då
Hsequential_245/instance_normalization_228/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2J
Hsequential_245/instance_normalization_228/moments/mean/reduction_indicesĀ
6sequential_245/instance_normalization_228/moments/meanMean)sequential_245/conv2d_135/Conv2D:output:0Qsequential_245/instance_normalization_228/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(28
6sequential_245/instance_normalization_228/moments/mean
>sequential_245/instance_normalization_228/moments/StopGradientStopGradient?sequential_245/instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2@
>sequential_245/instance_normalization_228/moments/StopGradientĪ
Csequential_245/instance_normalization_228/moments/SquaredDifferenceSquaredDifference)sequential_245/conv2d_135/Conv2D:output:0Gsequential_245/instance_normalization_228/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2E
Csequential_245/instance_normalization_228/moments/SquaredDifferenceķ
Lsequential_245/instance_normalization_228/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2N
Lsequential_245/instance_normalization_228/moments/variance/reduction_indicesģ
:sequential_245/instance_normalization_228/moments/varianceMeanGsequential_245/instance_normalization_228/moments/SquaredDifference:z:0Usequential_245/instance_normalization_228/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2<
:sequential_245/instance_normalization_228/moments/variance§
/sequential_245/instance_normalization_228/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'721
/sequential_245/instance_normalization_228/add/y”
-sequential_245/instance_normalization_228/addAddV2Csequential_245/instance_normalization_228/moments/variance:output:08sequential_245/instance_normalization_228/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2/
-sequential_245/instance_normalization_228/addŁ
/sequential_245/instance_normalization_228/RsqrtRsqrt1sequential_245/instance_normalization_228/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’21
/sequential_245/instance_normalization_228/Rsqrt
-sequential_245/instance_normalization_228/subSub)sequential_245/conv2d_135/Conv2D:output:0?sequential_245/instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2/
-sequential_245/instance_normalization_228/sub
-sequential_245/instance_normalization_228/mulMul1sequential_245/instance_normalization_228/sub:z:03sequential_245/instance_normalization_228/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  2/
-sequential_245/instance_normalization_228/muló
8sequential_245/instance_normalization_228/ReadVariableOpReadVariableOpAsequential_245_instance_normalization_228_readvariableop_resource*
_output_shapes	
:*
dtype02:
8sequential_245/instance_normalization_228/ReadVariableOp
/sequential_245/instance_normalization_228/mul_1Mul@sequential_245/instance_normalization_228/ReadVariableOp:value:01sequential_245/instance_normalization_228/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  21
/sequential_245/instance_normalization_228/mul_1
>sequential_245/instance_normalization_228/add_1/ReadVariableOpReadVariableOpGsequential_245_instance_normalization_228_add_1_readvariableop_resource*
_output_shapes	
:*
dtype02@
>sequential_245/instance_normalization_228/add_1/ReadVariableOp£
/sequential_245/instance_normalization_228/add_1AddV23sequential_245/instance_normalization_228/mul_1:z:0Fsequential_245/instance_normalization_228/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  21
/sequential_245/instance_normalization_228/add_1Ų
(sequential_245/leaky_re_lu_134/LeakyRelu	LeakyRelu3sequential_245/instance_normalization_228/add_1:z:0*0
_output_shapes
:’’’’’’’’’  *
alpha%>2*
(sequential_245/leaky_re_lu_134/LeakyReluÆ
zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_2/Pad/paddingsĪ
zero_padding2d_2/PadPad6sequential_245/leaky_re_lu_134/LeakyRelu:activations:0&zero_padding2d_2/Pad/paddings:output:0*
T0*0
_output_shapes
:’’’’’’’’’""2
zero_padding2d_2/Padø
 conv2d_136/Conv2D/ReadVariableOpReadVariableOp)conv2d_136_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype02"
 conv2d_136/Conv2D/ReadVariableOpŻ
conv2d_136/Conv2DConv2Dzero_padding2d_2/Pad:output:0(conv2d_136/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
conv2d_136/Conv2DĒ
9instance_normalization_229/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2;
9instance_normalization_229/moments/mean/reduction_indices
'instance_normalization_229/moments/meanMeanconv2d_136/Conv2D:output:0Binstance_normalization_229/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2)
'instance_normalization_229/moments/meanß
/instance_normalization_229/moments/StopGradientStopGradient0instance_normalization_229/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’21
/instance_normalization_229/moments/StopGradient
4instance_normalization_229/moments/SquaredDifferenceSquaredDifferenceconv2d_136/Conv2D:output:08instance_normalization_229/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’26
4instance_normalization_229/moments/SquaredDifferenceĻ
=instance_normalization_229/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2?
=instance_normalization_229/moments/variance/reduction_indices°
+instance_normalization_229/moments/varianceMean8instance_normalization_229/moments/SquaredDifference:z:0Finstance_normalization_229/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2-
+instance_normalization_229/moments/variance
 instance_normalization_229/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72"
 instance_normalization_229/add/yå
instance_normalization_229/addAddV24instance_normalization_229/moments/variance:output:0)instance_normalization_229/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_229/add¬
 instance_normalization_229/RsqrtRsqrt"instance_normalization_229/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_229/RsqrtŠ
instance_normalization_229/subSubconv2d_136/Conv2D:output:00instance_normalization_229/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_229/subĢ
instance_normalization_229/mulMul"instance_normalization_229/sub:z:0$instance_normalization_229/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_229/mulĘ
)instance_normalization_229/ReadVariableOpReadVariableOp2instance_normalization_229_readvariableop_resource*
_output_shapes	
:*
dtype02+
)instance_normalization_229/ReadVariableOpŻ
 instance_normalization_229/mul_1Mul1instance_normalization_229/ReadVariableOp:value:0"instance_normalization_229/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_229/mul_1Ų
/instance_normalization_229/add_1/ReadVariableOpReadVariableOp8instance_normalization_229_add_1_readvariableop_resource*
_output_shapes	
:*
dtype021
/instance_normalization_229/add_1/ReadVariableOpē
 instance_normalization_229/add_1AddV2$instance_normalization_229/mul_1:z:07instance_normalization_229/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_229/add_1«
leaky_re_lu_135/LeakyRelu	LeakyRelu$instance_normalization_229/add_1:z:0*0
_output_shapes
:’’’’’’’’’*
alpha%>2
leaky_re_lu_135/LeakyReluÆ
zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_3/Pad/paddingsæ
zero_padding2d_3/PadPad'leaky_re_lu_135/LeakyRelu:activations:0&zero_padding2d_3/Pad/paddings:output:0*
T0*0
_output_shapes
:’’’’’’’’’!!2
zero_padding2d_3/Pad·
 conv2d_137/Conv2D/ReadVariableOpReadVariableOp)conv2d_137_conv2d_readvariableop_resource*'
_output_shapes
:*
dtype02"
 conv2d_137/Conv2D/ReadVariableOpÜ
conv2d_137/Conv2DConv2Dzero_padding2d_3/Pad:output:0(conv2d_137/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
conv2d_137/Conv2D­
!conv2d_137/BiasAdd/ReadVariableOpReadVariableOp*conv2d_137_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02#
!conv2d_137/BiasAdd/ReadVariableOp“
conv2d_137/BiasAddBiasAddconv2d_137/Conv2D:output:0)conv2d_137/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:’’’’’’’’’2
conv2d_137/BiasAddĶ
IdentityIdentityconv2d_137/BiasAdd:output:0!^conv2d_136/Conv2D/ReadVariableOp"^conv2d_137/BiasAdd/ReadVariableOp!^conv2d_137/Conv2D/ReadVariableOp*^instance_normalization_229/ReadVariableOp0^instance_normalization_229/add_1/ReadVariableOp0^sequential_243/conv2d_133/Conv2D/ReadVariableOp0^sequential_244/conv2d_134/Conv2D/ReadVariableOp9^sequential_244/instance_normalization_227/ReadVariableOp?^sequential_244/instance_normalization_227/add_1/ReadVariableOp0^sequential_245/conv2d_135/Conv2D/ReadVariableOp9^sequential_245/instance_normalization_228/ReadVariableOp?^sequential_245/instance_normalization_228/add_1/ReadVariableOp*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2D
 conv2d_136/Conv2D/ReadVariableOp conv2d_136/Conv2D/ReadVariableOp2F
!conv2d_137/BiasAdd/ReadVariableOp!conv2d_137/BiasAdd/ReadVariableOp2D
 conv2d_137/Conv2D/ReadVariableOp conv2d_137/Conv2D/ReadVariableOp2V
)instance_normalization_229/ReadVariableOp)instance_normalization_229/ReadVariableOp2b
/instance_normalization_229/add_1/ReadVariableOp/instance_normalization_229/add_1/ReadVariableOp2b
/sequential_243/conv2d_133/Conv2D/ReadVariableOp/sequential_243/conv2d_133/Conv2D/ReadVariableOp2b
/sequential_244/conv2d_134/Conv2D/ReadVariableOp/sequential_244/conv2d_134/Conv2D/ReadVariableOp2t
8sequential_244/instance_normalization_227/ReadVariableOp8sequential_244/instance_normalization_227/ReadVariableOp2
>sequential_244/instance_normalization_227/add_1/ReadVariableOp>sequential_244/instance_normalization_227/add_1/ReadVariableOp2b
/sequential_245/conv2d_135/Conv2D/ReadVariableOp/sequential_245/conv2d_135/Conv2D/ReadVariableOp2t
8sequential_245/instance_normalization_228/ReadVariableOp8sequential_245/instance_normalization_228/ReadVariableOp2
>sequential_245/instance_normalization_228/add_1/ReadVariableOp>sequential_245/instance_normalization_228/add_1/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
Ī
ą
J__inference_sequential_245_layer_call_and_return_conditional_losses_151314
conv2d_135_input-
conv2d_135_151304:0
!instance_normalization_228_151307:	0
!instance_normalization_228_151309:	
identity¢"conv2d_135/StatefulPartitionedCall¢2instance_normalization_228/StatefulPartitionedCall
"conv2d_135/StatefulPartitionedCallStatefulPartitionedCallconv2d_135_inputconv2d_135_151304*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_135_layer_call_and_return_conditional_losses_1511702$
"conv2d_135/StatefulPartitionedCall
2instance_normalization_228/StatefulPartitionedCallStatefulPartitionedCall+conv2d_135/StatefulPartitionedCall:output:0!instance_normalization_228_151307!instance_normalization_228_151309*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_15119524
2instance_normalization_228/StatefulPartitionedCallŖ
leaky_re_lu_134/PartitionedCallPartitionedCall;instance_normalization_228/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_1512062!
leaky_re_lu_134/PartitionedCallß
IdentityIdentity(leaky_re_lu_134/PartitionedCall:output:0#^conv2d_135/StatefulPartitionedCall3^instance_normalization_228/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 2H
"conv2d_135/StatefulPartitionedCall"conv2d_135/StatefulPartitionedCall2h
2instance_normalization_228/StatefulPartitionedCall2instance_normalization_228/StatefulPartitionedCall:b ^
0
_output_shapes
:’’’’’’’’’@@
*
_user_specified_nameconv2d_135_input
Ø

+__inference_conv2d_136_layer_call_fn_152119

inputs#
unknown:
identity¢StatefulPartitionedCallõ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_136_layer_call_and_return_conditional_losses_1513722
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:’’’’’’’’’"": 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’""
 
_user_specified_nameinputs
¢
g
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_151206

inputs
identitym
	LeakyRelu	LeakyReluinputs*0
_output_shapes
:’’’’’’’’’  *
alpha%>2
	LeakyRelut
IdentityIdentityLeakyRelu:activations:0*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’  :X T
0
_output_shapes
:’’’’’’’’’  
 
_user_specified_nameinputs
Õ
”
;__inference_instance_normalization_227_layer_call_fn_152232
x
unknown:	
	unknown_0:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallxunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_1510372
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’@@: : 22
StatefulPartitionedCallStatefulPartitionedCall:S O
0
_output_shapes
:’’’’’’’’’@@

_user_specified_namex
§
Ś
)__inference_model_17_layer_call_fn_151613
input_image!
unknown:@$
	unknown_0:@
	unknown_1:	
	unknown_2:	%
	unknown_3:
	unknown_4:	
	unknown_5:	%
	unknown_6:
	unknown_7:	
	unknown_8:	$
	unknown_9:

unknown_10:
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinput_imageunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8 *M
fHRF
D__inference_model_17_layer_call_and_return_conditional_losses_1515572
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:^ Z
1
_output_shapes
:’’’’’’’’’
%
_user_specified_nameinput_image
0
ķ
D__inference_model_17_layer_call_and_return_conditional_losses_151428

inputs/
sequential_243_151347:@0
sequential_244_151350:@$
sequential_244_151352:	$
sequential_244_151354:	1
sequential_245_151357:$
sequential_245_151359:	$
sequential_245_151361:	-
conv2d_136_151373:0
!instance_normalization_229_151398:	0
!instance_normalization_229_151400:	,
conv2d_137_151422:
conv2d_137_151424:
identity¢"conv2d_136/StatefulPartitionedCall¢"conv2d_137/StatefulPartitionedCall¢2instance_normalization_229/StatefulPartitionedCall¢&sequential_243/StatefulPartitionedCall¢&sequential_244/StatefulPartitionedCall¢&sequential_245/StatefulPartitionedCall¦
&sequential_243/StatefulPartitionedCallStatefulPartitionedCallinputssequential_243_151347*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509342(
&sequential_243/StatefulPartitionedCall
&sequential_244/StatefulPartitionedCallStatefulPartitionedCall/sequential_243/StatefulPartitionedCall:output:0sequential_244_151350sequential_244_151352sequential_244_151354*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1510512(
&sequential_244/StatefulPartitionedCall
&sequential_245/StatefulPartitionedCallStatefulPartitionedCall/sequential_244/StatefulPartitionedCall:output:0sequential_245_151357sequential_245_151359sequential_245_151361*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512092(
&sequential_245/StatefulPartitionedCall”
 zero_padding2d_2/PartitionedCallPartitionedCall/sequential_245/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’""* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_1513212"
 zero_padding2d_2/PartitionedCallø
"conv2d_136/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_2/PartitionedCall:output:0conv2d_136_151373*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_136_layer_call_and_return_conditional_losses_1513722$
"conv2d_136/StatefulPartitionedCall
2instance_normalization_229/StatefulPartitionedCallStatefulPartitionedCall+conv2d_136/StatefulPartitionedCall:output:0!instance_normalization_229_151398!instance_normalization_229_151400*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_15139724
2instance_normalization_229/StatefulPartitionedCallŖ
leaky_re_lu_135/PartitionedCallPartitionedCall;instance_normalization_229/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_1514082!
leaky_re_lu_135/PartitionedCall
 zero_padding2d_3/PartitionedCallPartitionedCall(leaky_re_lu_135/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’!!* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_1513342"
 zero_padding2d_3/PartitionedCallĢ
"conv2d_137/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_3/PartitionedCall:output:0conv2d_137_151422conv2d_137_151424*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_137_layer_call_and_return_conditional_losses_1514212$
"conv2d_137/StatefulPartitionedCall
IdentityIdentity+conv2d_137/StatefulPartitionedCall:output:0#^conv2d_136/StatefulPartitionedCall#^conv2d_137/StatefulPartitionedCall3^instance_normalization_229/StatefulPartitionedCall'^sequential_243/StatefulPartitionedCall'^sequential_244/StatefulPartitionedCall'^sequential_245/StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2H
"conv2d_136/StatefulPartitionedCall"conv2d_136/StatefulPartitionedCall2H
"conv2d_137/StatefulPartitionedCall"conv2d_137/StatefulPartitionedCall2h
2instance_normalization_229/StatefulPartitionedCall2instance_normalization_229/StatefulPartitionedCall2P
&sequential_243/StatefulPartitionedCall&sequential_243/StatefulPartitionedCall2P
&sequential_244/StatefulPartitionedCall&sequential_244/StatefulPartitionedCall2P
&sequential_245/StatefulPartitionedCall&sequential_245/StatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
×'
¼
J__inference_sequential_244_layer_call_and_return_conditional_losses_152015

inputsD
)conv2d_134_conv2d_readvariableop_resource:@A
2instance_normalization_227_readvariableop_resource:	G
8instance_normalization_227_add_1_readvariableop_resource:	
identity¢ conv2d_134/Conv2D/ReadVariableOp¢)instance_normalization_227/ReadVariableOp¢/instance_normalization_227/add_1/ReadVariableOp·
 conv2d_134/Conv2D/ReadVariableOpReadVariableOp)conv2d_134_conv2d_readvariableop_resource*'
_output_shapes
:@*
dtype02"
 conv2d_134/Conv2D/ReadVariableOpÅ
conv2d_134/Conv2DConv2Dinputs(conv2d_134/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2
conv2d_134/Conv2DĒ
9instance_normalization_227/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2;
9instance_normalization_227/moments/mean/reduction_indices
'instance_normalization_227/moments/meanMeanconv2d_134/Conv2D:output:0Binstance_normalization_227/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2)
'instance_normalization_227/moments/meanß
/instance_normalization_227/moments/StopGradientStopGradient0instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’21
/instance_normalization_227/moments/StopGradient
4instance_normalization_227/moments/SquaredDifferenceSquaredDifferenceconv2d_134/Conv2D:output:08instance_normalization_227/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@26
4instance_normalization_227/moments/SquaredDifferenceĻ
=instance_normalization_227/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2?
=instance_normalization_227/moments/variance/reduction_indices°
+instance_normalization_227/moments/varianceMean8instance_normalization_227/moments/SquaredDifference:z:0Finstance_normalization_227/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2-
+instance_normalization_227/moments/variance
 instance_normalization_227/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72"
 instance_normalization_227/add/yå
instance_normalization_227/addAddV24instance_normalization_227/moments/variance:output:0)instance_normalization_227/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_227/add¬
 instance_normalization_227/RsqrtRsqrt"instance_normalization_227/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_227/RsqrtŠ
instance_normalization_227/subSubconv2d_134/Conv2D:output:00instance_normalization_227/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2 
instance_normalization_227/subĢ
instance_normalization_227/mulMul"instance_normalization_227/sub:z:0$instance_normalization_227/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@2 
instance_normalization_227/mulĘ
)instance_normalization_227/ReadVariableOpReadVariableOp2instance_normalization_227_readvariableop_resource*
_output_shapes	
:*
dtype02+
)instance_normalization_227/ReadVariableOpŻ
 instance_normalization_227/mul_1Mul1instance_normalization_227/ReadVariableOp:value:0"instance_normalization_227/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@2"
 instance_normalization_227/mul_1Ų
/instance_normalization_227/add_1/ReadVariableOpReadVariableOp8instance_normalization_227_add_1_readvariableop_resource*
_output_shapes	
:*
dtype021
/instance_normalization_227/add_1/ReadVariableOpē
 instance_normalization_227/add_1AddV2$instance_normalization_227/mul_1:z:07instance_normalization_227/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@2"
 instance_normalization_227/add_1«
leaky_re_lu_133/LeakyRelu	LeakyRelu$instance_normalization_227/add_1:z:0*0
_output_shapes
:’’’’’’’’’@@*
alpha%>2
leaky_re_lu_133/LeakyRelu
IdentityIdentity'leaky_re_lu_133/LeakyRelu:activations:0!^conv2d_134/Conv2D/ReadVariableOp*^instance_normalization_227/ReadVariableOp0^instance_normalization_227/add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 2D
 conv2d_134/Conv2D/ReadVariableOp conv2d_134/Conv2D/ReadVariableOp2V
)instance_normalization_227/ReadVariableOp)instance_normalization_227/ReadVariableOp2b
/instance_normalization_227/add_1/ReadVariableOp/instance_normalization_227/add_1/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
0
ķ
D__inference_model_17_layer_call_and_return_conditional_losses_151557

inputs/
sequential_243_151523:@0
sequential_244_151526:@$
sequential_244_151528:	$
sequential_244_151530:	1
sequential_245_151533:$
sequential_245_151535:	$
sequential_245_151537:	-
conv2d_136_151541:0
!instance_normalization_229_151544:	0
!instance_normalization_229_151546:	,
conv2d_137_151551:
conv2d_137_151553:
identity¢"conv2d_136/StatefulPartitionedCall¢"conv2d_137/StatefulPartitionedCall¢2instance_normalization_229/StatefulPartitionedCall¢&sequential_243/StatefulPartitionedCall¢&sequential_244/StatefulPartitionedCall¢&sequential_245/StatefulPartitionedCall¦
&sequential_243/StatefulPartitionedCallStatefulPartitionedCallinputssequential_243_151523*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509702(
&sequential_243/StatefulPartitionedCall
&sequential_244/StatefulPartitionedCallStatefulPartitionedCall/sequential_243/StatefulPartitionedCall:output:0sequential_244_151526sequential_244_151528sequential_244_151530*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1511102(
&sequential_244/StatefulPartitionedCall
&sequential_245/StatefulPartitionedCallStatefulPartitionedCall/sequential_244/StatefulPartitionedCall:output:0sequential_245_151533sequential_245_151535sequential_245_151537*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512682(
&sequential_245/StatefulPartitionedCall”
 zero_padding2d_2/PartitionedCallPartitionedCall/sequential_245/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’""* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_1513212"
 zero_padding2d_2/PartitionedCallø
"conv2d_136/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_2/PartitionedCall:output:0conv2d_136_151541*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_136_layer_call_and_return_conditional_losses_1513722$
"conv2d_136/StatefulPartitionedCall
2instance_normalization_229/StatefulPartitionedCallStatefulPartitionedCall+conv2d_136/StatefulPartitionedCall:output:0!instance_normalization_229_151544!instance_normalization_229_151546*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_15139724
2instance_normalization_229/StatefulPartitionedCallŖ
leaky_re_lu_135/PartitionedCallPartitionedCall;instance_normalization_229/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_1514082!
leaky_re_lu_135/PartitionedCall
 zero_padding2d_3/PartitionedCallPartitionedCall(leaky_re_lu_135/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’!!* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_1513342"
 zero_padding2d_3/PartitionedCallĢ
"conv2d_137/StatefulPartitionedCallStatefulPartitionedCall)zero_padding2d_3/PartitionedCall:output:0conv2d_137_151551conv2d_137_151553*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_137_layer_call_and_return_conditional_losses_1514212$
"conv2d_137/StatefulPartitionedCall
IdentityIdentity+conv2d_137/StatefulPartitionedCall:output:0#^conv2d_136/StatefulPartitionedCall#^conv2d_137/StatefulPartitionedCall3^instance_normalization_229/StatefulPartitionedCall'^sequential_243/StatefulPartitionedCall'^sequential_244/StatefulPartitionedCall'^sequential_245/StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 2H
"conv2d_136/StatefulPartitionedCall"conv2d_136/StatefulPartitionedCall2H
"conv2d_137/StatefulPartitionedCall"conv2d_137/StatefulPartitionedCall2h
2instance_normalization_229/StatefulPartitionedCall2instance_normalization_229/StatefulPartitionedCall2P
&sequential_243/StatefulPartitionedCall&sequential_243/StatefulPartitionedCall2P
&sequential_244/StatefulPartitionedCall&sequential_244/StatefulPartitionedCall2P
&sequential_245/StatefulPartitionedCall&sequential_245/StatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
¢
g
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_152317

inputs
identitym
	LeakyRelu	LeakyReluinputs*0
_output_shapes
:’’’’’’’’’  *
alpha%>2
	LeakyRelut
IdentityIdentityLeakyRelu:activations:0*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’  :X T
0
_output_shapes
:’’’’’’’’’  
 
_user_specified_nameinputs
Ä	
Ń
J__inference_sequential_243_layer_call_and_return_conditional_losses_151960

inputsC
)conv2d_133_conv2d_readvariableop_resource:@
identity¢ conv2d_133/Conv2D/ReadVariableOp¶
 conv2d_133/Conv2D/ReadVariableOpReadVariableOp)conv2d_133_conv2d_readvariableop_resource*&
_output_shapes
:@*
dtype02"
 conv2d_133/Conv2D/ReadVariableOpĘ
conv2d_133/Conv2DConv2Dinputs(conv2d_133/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2
conv2d_133/Conv2D¢
leaky_re_lu_132/LeakyRelu	LeakyReluconv2d_133/Conv2D:output:0*1
_output_shapes
:’’’’’’’’’@*
alpha%>2
leaky_re_lu_132/LeakyReluØ
IdentityIdentity'leaky_re_lu_132/LeakyRelu:activations:0!^conv2d_133/Conv2D/ReadVariableOp*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2D
 conv2d_133/Conv2D/ReadVariableOp conv2d_133/Conv2D/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs

Õ
)__inference_model_17_layer_call_fn_151776

inputs!
unknown:@$
	unknown_0:@
	unknown_1:	
	unknown_2:	%
	unknown_3:
	unknown_4:	
	unknown_5:	%
	unknown_6:
	unknown_7:	
	unknown_8:	$
	unknown_9:

unknown_10:
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8 *M
fHRF
D__inference_model_17_layer_call_and_return_conditional_losses_1515572
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs

·
F__inference_conv2d_133_layer_call_and_return_conditional_losses_150922

inputs8
conv2d_readvariableop_resource:@
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:@*
dtype02
Conv2D/ReadVariableOp„
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
Õ
”
;__inference_instance_normalization_228_layer_call_fn_152286
x
unknown:	
	unknown_0:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallxunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_1511952
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’  : : 22
StatefulPartitionedCallStatefulPartitionedCall:S O
0
_output_shapes
:’’’’’’’’’  

_user_specified_namex
§
Ś
)__inference_model_17_layer_call_fn_151455
input_image!
unknown:@$
	unknown_0:@
	unknown_1:	
	unknown_2:	%
	unknown_3:
	unknown_4:	
	unknown_5:	%
	unknown_6:
	unknown_7:	
	unknown_8:	$
	unknown_9:

unknown_10:
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinput_imageunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8 *M
fHRF
D__inference_model_17_layer_call_and_return_conditional_losses_1514282
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:^ Z
1
_output_shapes
:’’’’’’’’’
%
_user_specified_nameinput_image
ß
ī
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_151037
x&
readvariableop_resource:	,
add_1_readvariableop_resource:	
identity¢ReadVariableOp¢add_1/ReadVariableOp
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2 
moments/mean/reduction_indices
moments/meanMeanx'moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/mean
moments/StopGradientStopGradientmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/StopGradientØ
moments/SquaredDifferenceSquaredDifferencexmoments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
moments/SquaredDifference
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2$
"moments/variance/reduction_indicesÄ
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/varianceS
add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72
add/yy
addAddV2moments/variance:output:0add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
add[
RsqrtRsqrtadd:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
Rsqrtf
subSubxmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
sub`
mulMulsub:z:0	Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
mulu
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:*
dtype02
ReadVariableOpq
mul_1MulReadVariableOp:value:0mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
mul_1
add_1/ReadVariableOpReadVariableOpadd_1_readvariableop_resource*
_output_shapes	
:*
dtype02
add_1/ReadVariableOp{
add_1AddV2	mul_1:z:0add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@2
add_1
IdentityIdentity	add_1:z:0^ReadVariableOp^add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’@@: : 2 
ReadVariableOpReadVariableOp2,
add_1/ReadVariableOpadd_1/ReadVariableOp:S O
0
_output_shapes
:’’’’’’’’’@@

_user_specified_namex

Į
/__inference_sequential_245_layer_call_fn_152051

inputs#
unknown:
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512092
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs

¹
F__inference_conv2d_136_layer_call_and_return_conditional_losses_152126

inputs:
conv2d_readvariableop_resource:
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*(
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp„
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’*
paddingVALID*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:’’’’’’’’’"": 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’""
 
_user_specified_nameinputs
¢
g
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_152166

inputs
identitym
	LeakyRelu	LeakyReluinputs*0
_output_shapes
:’’’’’’’’’*
alpha%>2
	LeakyRelut
IdentityIdentityLeakyRelu:activations:0*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’:X T
0
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
 
Ė
/__inference_sequential_245_layer_call_fn_151218
conv2d_135_input#
unknown:
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallconv2d_135_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_245_layer_call_and_return_conditional_losses_1512092
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:b ^
0
_output_shapes
:’’’’’’’’’@@
*
_user_specified_nameconv2d_135_input
ć
Å
J__inference_sequential_243_layer_call_and_return_conditional_losses_150998
conv2d_133_input+
conv2d_133_150993:@
identity¢"conv2d_133/StatefulPartitionedCall 
"conv2d_133/StatefulPartitionedCallStatefulPartitionedCallconv2d_133_inputconv2d_133_150993*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_133_layer_call_and_return_conditional_losses_1509222$
"conv2d_133/StatefulPartitionedCall
leaky_re_lu_132/PartitionedCallPartitionedCall+conv2d_133/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_1509312!
leaky_re_lu_132/PartitionedCall«
IdentityIdentity(leaky_re_lu_132/PartitionedCall:output:0#^conv2d_133/StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2H
"conv2d_133/StatefulPartitionedCall"conv2d_133/StatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’
*
_user_specified_nameconv2d_133_input
ń
L
0__inference_leaky_re_lu_135_layer_call_fn_152161

inputs
identityÕ
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_1514082
PartitionedCallu
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’:X T
0
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
×'
½
J__inference_sequential_245_layer_call_and_return_conditional_losses_152087

inputsE
)conv2d_135_conv2d_readvariableop_resource:A
2instance_normalization_228_readvariableop_resource:	G
8instance_normalization_228_add_1_readvariableop_resource:	
identity¢ conv2d_135/Conv2D/ReadVariableOp¢)instance_normalization_228/ReadVariableOp¢/instance_normalization_228/add_1/ReadVariableOpø
 conv2d_135/Conv2D/ReadVariableOpReadVariableOp)conv2d_135_conv2d_readvariableop_resource*(
_output_shapes
:*
dtype02"
 conv2d_135/Conv2D/ReadVariableOpÅ
conv2d_135/Conv2DConv2Dinputs(conv2d_135/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2
conv2d_135/Conv2DĒ
9instance_normalization_228/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2;
9instance_normalization_228/moments/mean/reduction_indices
'instance_normalization_228/moments/meanMeanconv2d_135/Conv2D:output:0Binstance_normalization_228/moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2)
'instance_normalization_228/moments/meanß
/instance_normalization_228/moments/StopGradientStopGradient0instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’21
/instance_normalization_228/moments/StopGradient
4instance_normalization_228/moments/SquaredDifferenceSquaredDifferenceconv2d_135/Conv2D:output:08instance_normalization_228/moments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’  26
4instance_normalization_228/moments/SquaredDifferenceĻ
=instance_normalization_228/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2?
=instance_normalization_228/moments/variance/reduction_indices°
+instance_normalization_228/moments/varianceMean8instance_normalization_228/moments/SquaredDifference:z:0Finstance_normalization_228/moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2-
+instance_normalization_228/moments/variance
 instance_normalization_228/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72"
 instance_normalization_228/add/yå
instance_normalization_228/addAddV24instance_normalization_228/moments/variance:output:0)instance_normalization_228/add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2 
instance_normalization_228/add¬
 instance_normalization_228/RsqrtRsqrt"instance_normalization_228/add:z:0*
T0*0
_output_shapes
:’’’’’’’’’2"
 instance_normalization_228/RsqrtŠ
instance_normalization_228/subSubconv2d_135/Conv2D:output:00instance_normalization_228/moments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’  2 
instance_normalization_228/subĢ
instance_normalization_228/mulMul"instance_normalization_228/sub:z:0$instance_normalization_228/Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’  2 
instance_normalization_228/mulĘ
)instance_normalization_228/ReadVariableOpReadVariableOp2instance_normalization_228_readvariableop_resource*
_output_shapes	
:*
dtype02+
)instance_normalization_228/ReadVariableOpŻ
 instance_normalization_228/mul_1Mul1instance_normalization_228/ReadVariableOp:value:0"instance_normalization_228/mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’  2"
 instance_normalization_228/mul_1Ų
/instance_normalization_228/add_1/ReadVariableOpReadVariableOp8instance_normalization_228_add_1_readvariableop_resource*
_output_shapes	
:*
dtype021
/instance_normalization_228/add_1/ReadVariableOpē
 instance_normalization_228/add_1AddV2$instance_normalization_228/mul_1:z:07instance_normalization_228/add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  2"
 instance_normalization_228/add_1«
leaky_re_lu_134/LeakyRelu	LeakyRelu$instance_normalization_228/add_1:z:0*0
_output_shapes
:’’’’’’’’’  *
alpha%>2
leaky_re_lu_134/LeakyRelu
IdentityIdentity'leaky_re_lu_134/LeakyRelu:activations:0!^conv2d_135/Conv2D/ReadVariableOp*^instance_normalization_228/ReadVariableOp0^instance_normalization_228/add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 2D
 conv2d_135/Conv2D/ReadVariableOp conv2d_135/Conv2D/ReadVariableOp2V
)instance_normalization_228/ReadVariableOp)instance_normalization_228/ReadVariableOp2b
/instance_normalization_228/add_1/ReadVariableOp/instance_normalization_228/add_1/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs

Õ
)__inference_model_17_layer_call_fn_151747

inputs!
unknown:@$
	unknown_0:@
	unknown_1:	
	unknown_2:	%
	unknown_3:
	unknown_4:	
	unknown_5:	%
	unknown_6:
	unknown_7:	
	unknown_8:	$
	unknown_9:

unknown_10:
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:’’’’’’’’’*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8 *M
fHRF
D__inference_model_17_layer_call_and_return_conditional_losses_1514282
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:’’’’’’’’’: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs

·
F__inference_conv2d_133_layer_call_and_return_conditional_losses_152199

inputs8
conv2d_readvariableop_resource:@
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:@*
dtype02
Conv2D/ReadVariableOp„
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:’’’’’’’’’@*
paddingSAME*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
¦
g
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_152209

inputs
identityn
	LeakyRelu	LeakyReluinputs*1
_output_shapes
:’’’’’’’’’@*
alpha%>2
	LeakyReluu
IdentityIdentityLeakyRelu:activations:0*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:’’’’’’’’’@:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
õ
L
0__inference_leaky_re_lu_132_layer_call_fn_152204

inputs
identityÖ
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_1509312
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:’’’’’’’’’@:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs

ø
F__inference_conv2d_134_layer_call_and_return_conditional_losses_152223

inputs9
conv2d_readvariableop_resource:@
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
:@*
dtype02
Conv2D/ReadVariableOp¤
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’@@*
paddingSAME*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’@: 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
ń
L
0__inference_leaky_re_lu_133_layer_call_fn_152258

inputs
identityÕ
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_1510482
PartitionedCallu
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:’’’’’’’’’@@:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
Ü
M
1__inference_zero_padding2d_2_layer_call_fn_151327

inputs
identityš
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *U
fPRN
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_1513212
PartitionedCall
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’:r n
J
_output_shapes8
6:4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
 
_user_specified_nameinputs
²

/__inference_sequential_243_layer_call_fn_151945

inputs!
unknown:@
identity¢StatefulPartitionedCallś
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_243_layer_call_and_return_conditional_losses_1509342
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’
 
_user_specified_nameinputs
Õ
”
;__inference_instance_normalization_229_layer_call_fn_152135
x
unknown:	
	unknown_0:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallxunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_1513972
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’: : 22
StatefulPartitionedCallStatefulPartitionedCall:S O
0
_output_shapes
:’’’’’’’’’

_user_specified_namex
¦
g
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_150931

inputs
identityn
	LeakyRelu	LeakyReluinputs*1
_output_shapes
:’’’’’’’’’@*
alpha%>2
	LeakyReluu
IdentityIdentityLeakyRelu:activations:0*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:’’’’’’’’’@:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs
±
Õ
J__inference_sequential_244_layer_call_and_return_conditional_losses_151110

inputs,
conv2d_134_151100:@0
!instance_normalization_227_151103:	0
!instance_normalization_227_151105:	
identity¢"conv2d_134/StatefulPartitionedCall¢2instance_normalization_227/StatefulPartitionedCall
"conv2d_134/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_134_151100*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_134_layer_call_and_return_conditional_losses_1510122$
"conv2d_134/StatefulPartitionedCall
2instance_normalization_227/StatefulPartitionedCallStatefulPartitionedCall+conv2d_134/StatefulPartitionedCall:output:0!instance_normalization_227_151103!instance_normalization_227_151105*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_15103724
2instance_normalization_227/StatefulPartitionedCallŖ
leaky_re_lu_133/PartitionedCallPartitionedCall;instance_normalization_227/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_1510482!
leaky_re_lu_133/PartitionedCallß
IdentityIdentity(leaky_re_lu_133/PartitionedCall:output:0#^conv2d_134/StatefulPartitionedCall3^instance_normalization_227/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 2H
"conv2d_134/StatefulPartitionedCall"conv2d_134/StatefulPartitionedCall2h
2instance_normalization_227/StatefulPartitionedCall2instance_normalization_227/StatefulPartitionedCall:Y U
1
_output_shapes
:’’’’’’’’’@
 
_user_specified_nameinputs

¹
F__inference_conv2d_135_layer_call_and_return_conditional_losses_152277

inputs:
conv2d_readvariableop_resource:
identity¢Conv2D/ReadVariableOp
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*(
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp¤
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’  *
paddingSAME*
strides
2
Conv2D
IdentityIdentityConv2D:output:0^Conv2D/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:’’’’’’’’’@@: 2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:X T
0
_output_shapes
:’’’’’’’’’@@
 
_user_specified_nameinputs
Ī
ą
J__inference_sequential_245_layer_call_and_return_conditional_losses_151301
conv2d_135_input-
conv2d_135_151291:0
!instance_normalization_228_151294:	0
!instance_normalization_228_151296:	
identity¢"conv2d_135/StatefulPartitionedCall¢2instance_normalization_228/StatefulPartitionedCall
"conv2d_135/StatefulPartitionedCallStatefulPartitionedCallconv2d_135_inputconv2d_135_151291*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_135_layer_call_and_return_conditional_losses_1511702$
"conv2d_135/StatefulPartitionedCall
2instance_normalization_228/StatefulPartitionedCallStatefulPartitionedCall+conv2d_135/StatefulPartitionedCall:output:0!instance_normalization_228_151294!instance_normalization_228_151296*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *_
fZRX
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_15119524
2instance_normalization_228/StatefulPartitionedCallŖ
leaky_re_lu_134/PartitionedCallPartitionedCall;instance_normalization_228/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’  * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_1512062!
leaky_re_lu_134/PartitionedCallß
IdentityIdentity(leaky_re_lu_134/PartitionedCall:output:0#^conv2d_135/StatefulPartitionedCall3^instance_normalization_228/StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’  2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":’’’’’’’’’@@: : : 2H
"conv2d_135/StatefulPartitionedCall"conv2d_135/StatefulPartitionedCall2h
2instance_normalization_228/StatefulPartitionedCall2instance_normalization_228/StatefulPartitionedCall:b ^
0
_output_shapes
:’’’’’’’’’@@
*
_user_specified_nameconv2d_135_input
”
Ź
/__inference_sequential_244_layer_call_fn_151130
conv2d_134_input"
unknown:@
	unknown_0:	
	unknown_1:	
identity¢StatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallconv2d_134_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:’’’’’’’’’@@*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *S
fNRL
J__inference_sequential_244_layer_call_and_return_conditional_losses_1511102
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*0
_output_shapes
:’’’’’’’’’@@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:’’’’’’’’’@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’@
*
_user_specified_nameconv2d_134_input
ß
ī
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_152156
x&
readvariableop_resource:	,
add_1_readvariableop_resource:	
identity¢ReadVariableOp¢add_1/ReadVariableOp
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2 
moments/mean/reduction_indices
moments/meanMeanx'moments/mean/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/mean
moments/StopGradientStopGradientmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/StopGradientØ
moments/SquaredDifferenceSquaredDifferencexmoments/StopGradient:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
moments/SquaredDifference
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      2$
"moments/variance/reduction_indicesÄ
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*0
_output_shapes
:’’’’’’’’’*
	keep_dims(2
moments/varianceS
add/yConst*
_output_shapes
: *
dtype0*
valueB
 *¬Å'72
add/yy
addAddV2moments/variance:output:0add/y:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
add[
RsqrtRsqrtadd:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
Rsqrtf
subSubxmoments/mean:output:0*
T0*0
_output_shapes
:’’’’’’’’’2
sub`
mulMulsub:z:0	Rsqrt:y:0*
T0*0
_output_shapes
:’’’’’’’’’2
mulu
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:*
dtype02
ReadVariableOpq
mul_1MulReadVariableOp:value:0mul:z:0*
T0*0
_output_shapes
:’’’’’’’’’2
mul_1
add_1/ReadVariableOpReadVariableOpadd_1_readvariableop_resource*
_output_shapes	
:*
dtype02
add_1/ReadVariableOp{
add_1AddV2	mul_1:z:0add_1/ReadVariableOp:value:0*
T0*0
_output_shapes
:’’’’’’’’’2
add_1
IdentityIdentity	add_1:z:0^ReadVariableOp^add_1/ReadVariableOp*
T0*0
_output_shapes
:’’’’’’’’’2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :’’’’’’’’’: : 2 
ReadVariableOpReadVariableOp2,
add_1/ReadVariableOpadd_1/ReadVariableOp:S O
0
_output_shapes
:’’’’’’’’’

_user_specified_namex
ć
Å
J__inference_sequential_243_layer_call_and_return_conditional_losses_150990
conv2d_133_input+
conv2d_133_150985:@
identity¢"conv2d_133/StatefulPartitionedCall 
"conv2d_133/StatefulPartitionedCallStatefulPartitionedCallconv2d_133_inputconv2d_133_150985*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8 *O
fJRH
F__inference_conv2d_133_layer_call_and_return_conditional_losses_1509222$
"conv2d_133/StatefulPartitionedCall
leaky_re_lu_132/PartitionedCallPartitionedCall+conv2d_133/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:’’’’’’’’’@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8 *T
fORM
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_1509312!
leaky_re_lu_132/PartitionedCall«
IdentityIdentity(leaky_re_lu_132/PartitionedCall:output:0#^conv2d_133/StatefulPartitionedCall*
T0*1
_output_shapes
:’’’’’’’’’@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:’’’’’’’’’: 2H
"conv2d_133/StatefulPartitionedCall"conv2d_133/StatefulPartitionedCall:c _
1
_output_shapes
:’’’’’’’’’
*
_user_specified_nameconv2d_133_input"ĢL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*Ē
serving_default³
M
input_image>
serving_default_input_image:0’’’’’’’’’F

conv2d_1378
StatefulPartitionedCall:0’’’’’’’’’tensorflow/serving/predict:ģ
@
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer-7
	layer-8

layer_with_weights-5

layer-9
	variables
regularization_losses
trainable_variables
	keras_api

signatures
Ć__call__
Ä_default_save_signature
+Å&call_and_return_all_conditional_losses"­<
_tf_keras_network<{"name": "model_17", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "Functional", "config": {"name": "model_17", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_image"}, "name": "input_image", "inbound_nodes": []}, {"class_name": "Sequential", "config": {"name": "sequential_243", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_133_input"}}, {"class_name": "Conv2D", "config": {"name": "conv2d_133", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_132", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}]}, "name": "sequential_243", "inbound_nodes": [[["input_image", 0, 0, {}]]]}, {"class_name": "Sequential", "config": {"name": "sequential_244", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 128, 128, 64]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_134_input"}}, {"class_name": "Conv2D", "config": {"name": "conv2d_134", "trainable": true, "dtype": "float32", "filters": 128, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "InstanceNormalization", "config": {"layer was saved without config": true}}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_133", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}]}, "name": "sequential_244", "inbound_nodes": [[["sequential_243", 1, 0, {}]]]}, {"class_name": "Sequential", "config": {"name": "sequential_245", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 64, 64, 128]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_135_input"}}, {"class_name": "Conv2D", "config": {"name": "conv2d_135", "trainable": true, "dtype": "float32", "filters": 256, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "InstanceNormalization", "config": {"layer was saved without config": true}}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_134", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}]}, "name": "sequential_245", "inbound_nodes": [[["sequential_244", 1, 0, {}]]]}, {"class_name": "ZeroPadding2D", "config": {"name": "zero_padding2d_2", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "name": "zero_padding2d_2", "inbound_nodes": [[["sequential_245", 1, 0, {}]]]}, {"class_name": "Conv2D", "config": {"name": "conv2d_136", "trainable": true, "dtype": "float32", "filters": 512, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 20}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "conv2d_136", "inbound_nodes": [[["zero_padding2d_2", 0, 0, {}]]]}, {"class_name": "InstanceNormalization", "config": {"layer was saved without config": true}, "name": "instance_normalization_229", "inbound_nodes": [[["conv2d_136", 0, 0, {}]]]}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_135", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "name": "leaky_re_lu_135", "inbound_nodes": [[["instance_normalization_229", 0, 0, {}]]]}, {"class_name": "ZeroPadding2D", "config": {"name": "zero_padding2d_3", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "name": "zero_padding2d_3", "inbound_nodes": [[["leaky_re_lu_135", 0, 0, {}]]]}, {"class_name": "Conv2D", "config": {"name": "conv2d_137", "trainable": true, "dtype": "float32", "filters": 1, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 20}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "name": "conv2d_137", "inbound_nodes": [[["zero_padding2d_3", 0, 0, {}]]]}], "input_layers": [["input_image", 0, 0]], "output_layers": [["conv2d_137", 0, 0]]}, "shared_object_id": 27, "input_spec": [{"class_name": "InputSpec", "config": {"dtype": null, "shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 256, 256, 3]}, "is_graph_network": true, "save_spec": {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 256, 256, 3]}, "float32", "input_image"]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "Functional"}}

_init_input_shape"
_tf_keras_input_layerā{"class_name": "InputLayer", "name": "input_image", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_image"}}

layer_with_weights-0
layer-0
layer-1
	variables
regularization_losses
trainable_variables
	keras_api
Ę__call__
+Ē&call_and_return_all_conditional_losses"Ó
_tf_keras_sequential“{"name": "sequential_243", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "Sequential", "config": {"name": "sequential_243", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_133_input"}}, {"class_name": "Conv2D", "config": {"name": "conv2d_133", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_132", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}]}, "inbound_nodes": [[["input_image", 0, 0, {}]]], "shared_object_id": 6, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 3}}, "shared_object_id": 29}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 256, 256, 3]}, "is_graph_network": true, "save_spec": {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 256, 256, 3]}, "float32", "conv2d_133_input"]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential_243", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 256, 256, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_133_input"}, "shared_object_id": 1}, {"class_name": "Conv2D", "config": {"name": "conv2d_133", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 2}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 3}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 4}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_132", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "shared_object_id": 5}]}}}
é
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer-2
	variables
regularization_losses
trainable_variables
	keras_api
Č__call__
+É&call_and_return_all_conditional_losses"ż
_tf_keras_sequentialŽ{"name": "sequential_244", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "Sequential", "config": {"name": "sequential_244", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 128, 128, 64]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_134_input"}}, {"class_name": "Conv2D", "config": {"name": "conv2d_134", "trainable": true, "dtype": "float32", "filters": 128, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "InstanceNormalization", "config": {"layer was saved without config": true}}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_133", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}]}, "inbound_nodes": [[["sequential_243", 1, 0, {}]]], "shared_object_id": 12, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 64}}, "shared_object_id": 30}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 128, 128, 64]}, "is_graph_network": true, "save_spec": {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 128, 128, 64]}, "float32", "conv2d_134_input"]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "Sequential"}}
ē
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
 layer-2
!	variables
"regularization_losses
#trainable_variables
$	keras_api
Ź__call__
+Ė&call_and_return_all_conditional_losses"ū
_tf_keras_sequentialÜ{"name": "sequential_245", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "Sequential", "config": {"name": "sequential_245", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 64, 64, 128]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "conv2d_135_input"}}, {"class_name": "Conv2D", "config": {"name": "conv2d_135", "trainable": true, "dtype": "float32", "filters": 256, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "InstanceNormalization", "config": {"layer was saved without config": true}}, {"class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_134", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}}]}, "inbound_nodes": [[["sequential_244", 1, 0, {}]]], "shared_object_id": 18, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 128}}, "shared_object_id": 31}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64, 64, 128]}, "is_graph_network": true, "save_spec": {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 64, 64, 128]}, "float32", "conv2d_135_input"]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "Sequential"}}
ī
%	variables
&regularization_losses
'trainable_variables
(	keras_api
Ģ__call__
+Ķ&call_and_return_all_conditional_losses"Ż
_tf_keras_layerĆ{"name": "zero_padding2d_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "ZeroPadding2D", "config": {"name": "zero_padding2d_2", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "inbound_nodes": [[["sequential_245", 1, 0, {}]]], "shared_object_id": 19, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}, "shared_object_id": 32}}
¦

)kernel
*	variables
+regularization_losses
,trainable_variables
-	keras_api
Ī__call__
+Ļ&call_and_return_all_conditional_losses"

_tf_keras_layerļ	{"name": "conv2d_136", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Conv2D", "config": {"name": "conv2d_136", "trainable": true, "dtype": "float32", "filters": 512, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 20}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 21}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "inbound_nodes": [[["zero_padding2d_2", 0, 0, {}]]], "shared_object_id": 22, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 256}}, "shared_object_id": 33}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 34, 34, 256]}}
¬
	.scale

/offset
0	variables
1regularization_losses
2trainable_variables
3	keras_api
Š__call__
+Ń&call_and_return_all_conditional_losses"
_tf_keras_layerź{"name": "instance_normalization_229", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "InstanceNormalization", "config": {"layer was saved without config": true}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 31, 31, 512]}}
»
4	variables
5regularization_losses
6trainable_variables
7	keras_api
Ņ__call__
+Ó&call_and_return_all_conditional_losses"Ŗ
_tf_keras_layer{"name": "leaky_re_lu_135", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_135", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "inbound_nodes": [[["instance_normalization_229", 0, 0, {}]]], "shared_object_id": 23}
ļ
8	variables
9regularization_losses
:trainable_variables
;	keras_api
Ō__call__
+Õ&call_and_return_all_conditional_losses"Ž
_tf_keras_layerÄ{"name": "zero_padding2d_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "ZeroPadding2D", "config": {"name": "zero_padding2d_3", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "inbound_nodes": [[["leaky_re_lu_135", 0, 0, {}]]], "shared_object_id": 24, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}, "shared_object_id": 34}}
­

<kernel
=bias
>	variables
?regularization_losses
@trainable_variables
A	keras_api
Ö__call__
+×&call_and_return_all_conditional_losses"

_tf_keras_layerģ	{"name": "conv2d_137", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Conv2D", "config": {"name": "conv2d_137", "trainable": true, "dtype": "float32", "filters": 1, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 20}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 25}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "inbound_nodes": [[["zero_padding2d_3", 0, 0, {}]]], "shared_object_id": 26, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 512}}, "shared_object_id": 35}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 33, 33, 512]}}
v
B0
C1
D2
E3
F4
G5
H6
)7
.8
/9
<10
=11"
trackable_list_wrapper
 "
trackable_list_wrapper
v
B0
C1
D2
E3
F4
G5
H6
)7
.8
/9
<10
=11"
trackable_list_wrapper
Ī
	variables
Inon_trainable_variables
regularization_losses
Jlayer_metrics
Kmetrics

Llayers
Mlayer_regularization_losses
trainable_variables
Ć__call__
Ä_default_save_signature
+Å&call_and_return_all_conditional_losses
'Å"call_and_return_conditional_losses"
_generic_user_object
-
Ųserving_default"
signature_map
 "
trackable_list_wrapper
ź


Bkernel
N	variables
Oregularization_losses
Ptrainable_variables
Q	keras_api
Ł__call__
+Ś&call_and_return_all_conditional_losses"Ķ	
_tf_keras_layer³	{"name": "conv2d_133", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Conv2D", "config": {"name": "conv2d_133", "trainable": true, "dtype": "float32", "filters": 64, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 2}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 3}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 4, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 3}}, "shared_object_id": 29}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 256, 256, 3]}}
ū
R	variables
Sregularization_losses
Ttrainable_variables
U	keras_api
Ū__call__
+Ü&call_and_return_all_conditional_losses"ź
_tf_keras_layerŠ{"name": "leaky_re_lu_132", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_132", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "shared_object_id": 5}
'
B0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
B0"
trackable_list_wrapper
°
	variables
Vnon_trainable_variables
regularization_losses
Wlayer_metrics
Xmetrics

Ylayers
Zlayer_regularization_losses
trainable_variables
Ę__call__
+Ē&call_and_return_all_conditional_losses
'Ē"call_and_return_conditional_losses"
_generic_user_object
ī


Ckernel
[	variables
\regularization_losses
]trainable_variables
^	keras_api
Ż__call__
+Ž&call_and_return_all_conditional_losses"Ń	
_tf_keras_layer·	{"name": "conv2d_134", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Conv2D", "config": {"name": "conv2d_134", "trainable": true, "dtype": "float32", "filters": 128, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 8}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 9}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 10, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 64}}, "shared_object_id": 30}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 128, 128, 64]}}
¬
	Dscale

Eoffset
_	variables
`regularization_losses
atrainable_variables
b	keras_api
ß__call__
+ą&call_and_return_all_conditional_losses"
_tf_keras_layerź{"name": "instance_normalization_227", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "InstanceNormalization", "config": {"layer was saved without config": true}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64, 64, 128]}}
ü
c	variables
dregularization_losses
etrainable_variables
f	keras_api
į__call__
+ā&call_and_return_all_conditional_losses"ė
_tf_keras_layerŃ{"name": "leaky_re_lu_133", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_133", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "shared_object_id": 11}
5
C0
D1
E2"
trackable_list_wrapper
 "
trackable_list_wrapper
5
C0
D1
E2"
trackable_list_wrapper
°
	variables
gnon_trainable_variables
regularization_losses
hlayer_metrics
imetrics

jlayers
klayer_regularization_losses
trainable_variables
Č__call__
+É&call_and_return_all_conditional_losses
'É"call_and_return_conditional_losses"
_generic_user_object
š


Fkernel
l	variables
mregularization_losses
ntrainable_variables
o	keras_api
ć__call__
+ä&call_and_return_all_conditional_losses"Ó	
_tf_keras_layer¹	{"name": "conv2d_135", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Conv2D", "config": {"name": "conv2d_135", "trainable": true, "dtype": "float32", "filters": 256, "kernel_size": {"class_name": "__tuple__", "items": [4, 4]}, "strides": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "same", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "RandomNormal", "config": {"mean": 0.0, "stddev": 0.02, "seed": null}, "shared_object_id": 14}, "bias_initializer": {"class_name": "Zeros", "config": {}, "shared_object_id": 15}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 16, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 128}}, "shared_object_id": 31}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 64, 64, 128]}}
¬
	Gscale

Hoffset
p	variables
qregularization_losses
rtrainable_variables
s	keras_api
å__call__
+ę&call_and_return_all_conditional_losses"
_tf_keras_layerź{"name": "instance_normalization_228", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "InstanceNormalization", "config": {"layer was saved without config": true}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 32, 32, 256]}}
ü
t	variables
uregularization_losses
vtrainable_variables
w	keras_api
ē__call__
+č&call_and_return_all_conditional_losses"ė
_tf_keras_layerŃ{"name": "leaky_re_lu_134", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "LeakyReLU", "config": {"name": "leaky_re_lu_134", "trainable": true, "dtype": "float32", "alpha": 0.30000001192092896}, "shared_object_id": 17}
5
F0
G1
H2"
trackable_list_wrapper
 "
trackable_list_wrapper
5
F0
G1
H2"
trackable_list_wrapper
°
!	variables
xnon_trainable_variables
"regularization_losses
ylayer_metrics
zmetrics

{layers
|layer_regularization_losses
#trainable_variables
Ź__call__
+Ė&call_and_return_all_conditional_losses
'Ė"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
²
%	variables
}non_trainable_variables
&regularization_losses
~layer_metrics
metrics
layers
 layer_regularization_losses
'trainable_variables
Ģ__call__
+Ķ&call_and_return_all_conditional_losses
'Ķ"call_and_return_conditional_losses"
_generic_user_object
-:+2conv2d_136/kernel
'
)0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
)0"
trackable_list_wrapper
µ
*	variables
non_trainable_variables
+regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
,trainable_variables
Ī__call__
+Ļ&call_and_return_all_conditional_losses
'Ļ"call_and_return_conditional_losses"
_generic_user_object
/:-2 instance_normalization_229/scale
0:.2!instance_normalization_229/offset
.
.0
/1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
.0
/1"
trackable_list_wrapper
µ
0	variables
non_trainable_variables
1regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
2trainable_variables
Š__call__
+Ń&call_and_return_all_conditional_losses
'Ń"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
µ
4	variables
non_trainable_variables
5regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
6trainable_variables
Ņ__call__
+Ó&call_and_return_all_conditional_losses
'Ó"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
µ
8	variables
non_trainable_variables
9regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
:trainable_variables
Ō__call__
+Õ&call_and_return_all_conditional_losses
'Õ"call_and_return_conditional_losses"
_generic_user_object
,:*2conv2d_137/kernel
:2conv2d_137/bias
.
<0
=1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
<0
=1"
trackable_list_wrapper
µ
>	variables
non_trainable_variables
?regularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
@trainable_variables
Ö__call__
+×&call_and_return_all_conditional_losses
'×"call_and_return_conditional_losses"
_generic_user_object
+:)@2conv2d_133/kernel
,:*@2conv2d_134/kernel
/:-2 instance_normalization_227/scale
0:.2!instance_normalization_227/offset
-:+2conv2d_135/kernel
/:-2 instance_normalization_228/scale
0:.2!instance_normalization_228/offset
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
f
0
1
2
3
4
5
6
7
	8

9"
trackable_list_wrapper
 "
trackable_list_wrapper
'
B0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
B0"
trackable_list_wrapper
µ
N	variables
non_trainable_variables
Oregularization_losses
layer_metrics
metrics
layers
 layer_regularization_losses
Ptrainable_variables
Ł__call__
+Ś&call_and_return_all_conditional_losses
'Ś"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
µ
R	variables
 non_trainable_variables
Sregularization_losses
”layer_metrics
¢metrics
£layers
 ¤layer_regularization_losses
Ttrainable_variables
Ū__call__
+Ü&call_and_return_all_conditional_losses
'Ü"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
'
C0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
C0"
trackable_list_wrapper
µ
[	variables
„non_trainable_variables
\regularization_losses
¦layer_metrics
§metrics
Ølayers
 ©layer_regularization_losses
]trainable_variables
Ż__call__
+Ž&call_and_return_all_conditional_losses
'Ž"call_and_return_conditional_losses"
_generic_user_object
.
D0
E1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
D0
E1"
trackable_list_wrapper
µ
_	variables
Ŗnon_trainable_variables
`regularization_losses
«layer_metrics
¬metrics
­layers
 ®layer_regularization_losses
atrainable_variables
ß__call__
+ą&call_and_return_all_conditional_losses
'ą"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
µ
c	variables
Ænon_trainable_variables
dregularization_losses
°layer_metrics
±metrics
²layers
 ³layer_regularization_losses
etrainable_variables
į__call__
+ā&call_and_return_all_conditional_losses
'ā"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
5
0
1
2"
trackable_list_wrapper
 "
trackable_list_wrapper
'
F0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
F0"
trackable_list_wrapper
µ
l	variables
“non_trainable_variables
mregularization_losses
µlayer_metrics
¶metrics
·layers
 ølayer_regularization_losses
ntrainable_variables
ć__call__
+ä&call_and_return_all_conditional_losses
'ä"call_and_return_conditional_losses"
_generic_user_object
.
G0
H1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
G0
H1"
trackable_list_wrapper
µ
p	variables
¹non_trainable_variables
qregularization_losses
ŗlayer_metrics
»metrics
¼layers
 ½layer_regularization_losses
rtrainable_variables
å__call__
+ę&call_and_return_all_conditional_losses
'ę"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
µ
t	variables
¾non_trainable_variables
uregularization_losses
ælayer_metrics
Ąmetrics
Įlayers
 Ālayer_regularization_losses
vtrainable_variables
ē__call__
+č&call_and_return_all_conditional_losses
'č"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
5
0
1
 2"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
ņ2ļ
)__inference_model_17_layer_call_fn_151455
)__inference_model_17_layer_call_fn_151747
)__inference_model_17_layer_call_fn_151776
)__inference_model_17_layer_call_fn_151613Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
ķ2ź
!__inference__wrapped_model_150908Ä
²
FullArgSpec
args 
varargsjargs
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *4¢1
/,
input_image’’’’’’’’’
Ž2Ū
D__inference_model_17_layer_call_and_return_conditional_losses_151857
D__inference_model_17_layer_call_and_return_conditional_losses_151938
D__inference_model_17_layer_call_and_return_conditional_losses_151650
D__inference_model_17_layer_call_and_return_conditional_losses_151687Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
2
/__inference_sequential_243_layer_call_fn_150939
/__inference_sequential_243_layer_call_fn_151945
/__inference_sequential_243_layer_call_fn_151952
/__inference_sequential_243_layer_call_fn_150982Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
ö2ó
J__inference_sequential_243_layer_call_and_return_conditional_losses_151960
J__inference_sequential_243_layer_call_and_return_conditional_losses_151968
J__inference_sequential_243_layer_call_and_return_conditional_losses_150990
J__inference_sequential_243_layer_call_and_return_conditional_losses_150998Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
2
/__inference_sequential_244_layer_call_fn_151060
/__inference_sequential_244_layer_call_fn_151979
/__inference_sequential_244_layer_call_fn_151990
/__inference_sequential_244_layer_call_fn_151130Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
ö2ó
J__inference_sequential_244_layer_call_and_return_conditional_losses_152015
J__inference_sequential_244_layer_call_and_return_conditional_losses_152040
J__inference_sequential_244_layer_call_and_return_conditional_losses_151143
J__inference_sequential_244_layer_call_and_return_conditional_losses_151156Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
2
/__inference_sequential_245_layer_call_fn_151218
/__inference_sequential_245_layer_call_fn_152051
/__inference_sequential_245_layer_call_fn_152062
/__inference_sequential_245_layer_call_fn_151288Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
ö2ó
J__inference_sequential_245_layer_call_and_return_conditional_losses_152087
J__inference_sequential_245_layer_call_and_return_conditional_losses_152112
J__inference_sequential_245_layer_call_and_return_conditional_losses_151301
J__inference_sequential_245_layer_call_and_return_conditional_losses_151314Ą
·²³
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsŖ 
annotationsŖ *
 
2
1__inference_zero_padding2d_2_layer_call_fn_151327ą
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *@¢=
;84’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
“2±
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_151321ą
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *@¢=
;84’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
Õ2Ņ
+__inference_conv2d_136_layer_call_fn_152119¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
š2ķ
F__inference_conv2d_136_layer_call_and_return_conditional_losses_152126¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ą2Ż
;__inference_instance_normalization_229_layer_call_fn_152135
²
FullArgSpec
args
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ū2ų
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_152156
²
FullArgSpec
args
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Ś2×
0__inference_leaky_re_lu_135_layer_call_fn_152161¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
õ2ņ
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_152166¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
2
1__inference_zero_padding2d_3_layer_call_fn_151340ą
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *@¢=
;84’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
“2±
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_151334ą
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *@¢=
;84’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
Õ2Ņ
+__inference_conv2d_137_layer_call_fn_152175¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
š2ķ
F__inference_conv2d_137_layer_call_and_return_conditional_losses_152185¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ĻBĢ
$__inference_signature_wrapper_151718input_image"
²
FullArgSpec
args 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Õ2Ņ
+__inference_conv2d_133_layer_call_fn_152192¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
š2ķ
F__inference_conv2d_133_layer_call_and_return_conditional_losses_152199¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Ś2×
0__inference_leaky_re_lu_132_layer_call_fn_152204¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
õ2ņ
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_152209¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Õ2Ņ
+__inference_conv2d_134_layer_call_fn_152216¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
š2ķ
F__inference_conv2d_134_layer_call_and_return_conditional_losses_152223¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ą2Ż
;__inference_instance_normalization_227_layer_call_fn_152232
²
FullArgSpec
args
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ū2ų
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_152253
²
FullArgSpec
args
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Ś2×
0__inference_leaky_re_lu_133_layer_call_fn_152258¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
õ2ņ
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_152263¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Õ2Ņ
+__inference_conv2d_135_layer_call_fn_152270¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
š2ķ
F__inference_conv2d_135_layer_call_and_return_conditional_losses_152277¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ą2Ż
;__inference_instance_normalization_228_layer_call_fn_152286
²
FullArgSpec
args
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
ū2ų
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_152307
²
FullArgSpec
args
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
Ś2×
0__inference_leaky_re_lu_134_layer_call_fn_152312¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 
õ2ņ
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_152317¢
²
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsŖ *
 µ
!__inference__wrapped_model_150908BCDEFGH)./<=>¢;
4¢1
/,
input_image’’’’’’’’’
Ŗ "?Ŗ<
:

conv2d_137,)

conv2d_137’’’’’’’’’¹
F__inference_conv2d_133_layer_call_and_return_conditional_losses_152199oB9¢6
/¢,
*'
inputs’’’’’’’’’
Ŗ "/¢,
%"
0’’’’’’’’’@
 
+__inference_conv2d_133_layer_call_fn_152192bB9¢6
/¢,
*'
inputs’’’’’’’’’
Ŗ ""’’’’’’’’’@ø
F__inference_conv2d_134_layer_call_and_return_conditional_losses_152223nC9¢6
/¢,
*'
inputs’’’’’’’’’@
Ŗ ".¢+
$!
0’’’’’’’’’@@
 
+__inference_conv2d_134_layer_call_fn_152216aC9¢6
/¢,
*'
inputs’’’’’’’’’@
Ŗ "!’’’’’’’’’@@·
F__inference_conv2d_135_layer_call_and_return_conditional_losses_152277mF8¢5
.¢+
)&
inputs’’’’’’’’’@@
Ŗ ".¢+
$!
0’’’’’’’’’  
 
+__inference_conv2d_135_layer_call_fn_152270`F8¢5
.¢+
)&
inputs’’’’’’’’’@@
Ŗ "!’’’’’’’’’  ·
F__inference_conv2d_136_layer_call_and_return_conditional_losses_152126m)8¢5
.¢+
)&
inputs’’’’’’’’’""
Ŗ ".¢+
$!
0’’’’’’’’’
 
+__inference_conv2d_136_layer_call_fn_152119`)8¢5
.¢+
)&
inputs’’’’’’’’’""
Ŗ "!’’’’’’’’’·
F__inference_conv2d_137_layer_call_and_return_conditional_losses_152185m<=8¢5
.¢+
)&
inputs’’’’’’’’’!!
Ŗ "-¢*
# 
0’’’’’’’’’
 
+__inference_conv2d_137_layer_call_fn_152175`<=8¢5
.¢+
)&
inputs’’’’’’’’’!!
Ŗ " ’’’’’’’’’Ć
V__inference_instance_normalization_227_layer_call_and_return_conditional_losses_152253iDE3¢0
)¢&
$!
x’’’’’’’’’@@
Ŗ ".¢+
$!
0’’’’’’’’’@@
 
;__inference_instance_normalization_227_layer_call_fn_152232\DE3¢0
)¢&
$!
x’’’’’’’’’@@
Ŗ "!’’’’’’’’’@@Ć
V__inference_instance_normalization_228_layer_call_and_return_conditional_losses_152307iGH3¢0
)¢&
$!
x’’’’’’’’’  
Ŗ ".¢+
$!
0’’’’’’’’’  
 
;__inference_instance_normalization_228_layer_call_fn_152286\GH3¢0
)¢&
$!
x’’’’’’’’’  
Ŗ "!’’’’’’’’’  Ć
V__inference_instance_normalization_229_layer_call_and_return_conditional_losses_152156i./3¢0
)¢&
$!
x’’’’’’’’’
Ŗ ".¢+
$!
0’’’’’’’’’
 
;__inference_instance_normalization_229_layer_call_fn_152135\./3¢0
)¢&
$!
x’’’’’’’’’
Ŗ "!’’’’’’’’’»
K__inference_leaky_re_lu_132_layer_call_and_return_conditional_losses_152209l9¢6
/¢,
*'
inputs’’’’’’’’’@
Ŗ "/¢,
%"
0’’’’’’’’’@
 
0__inference_leaky_re_lu_132_layer_call_fn_152204_9¢6
/¢,
*'
inputs’’’’’’’’’@
Ŗ ""’’’’’’’’’@¹
K__inference_leaky_re_lu_133_layer_call_and_return_conditional_losses_152263j8¢5
.¢+
)&
inputs’’’’’’’’’@@
Ŗ ".¢+
$!
0’’’’’’’’’@@
 
0__inference_leaky_re_lu_133_layer_call_fn_152258]8¢5
.¢+
)&
inputs’’’’’’’’’@@
Ŗ "!’’’’’’’’’@@¹
K__inference_leaky_re_lu_134_layer_call_and_return_conditional_losses_152317j8¢5
.¢+
)&
inputs’’’’’’’’’  
Ŗ ".¢+
$!
0’’’’’’’’’  
 
0__inference_leaky_re_lu_134_layer_call_fn_152312]8¢5
.¢+
)&
inputs’’’’’’’’’  
Ŗ "!’’’’’’’’’  ¹
K__inference_leaky_re_lu_135_layer_call_and_return_conditional_losses_152166j8¢5
.¢+
)&
inputs’’’’’’’’’
Ŗ ".¢+
$!
0’’’’’’’’’
 
0__inference_leaky_re_lu_135_layer_call_fn_152161]8¢5
.¢+
)&
inputs’’’’’’’’’
Ŗ "!’’’’’’’’’Ī
D__inference_model_17_layer_call_and_return_conditional_losses_151650BCDEFGH)./<=F¢C
<¢9
/,
input_image’’’’’’’’’
p 

 
Ŗ "-¢*
# 
0’’’’’’’’’
 Ī
D__inference_model_17_layer_call_and_return_conditional_losses_151687BCDEFGH)./<=F¢C
<¢9
/,
input_image’’’’’’’’’
p

 
Ŗ "-¢*
# 
0’’’’’’’’’
 É
D__inference_model_17_layer_call_and_return_conditional_losses_151857BCDEFGH)./<=A¢>
7¢4
*'
inputs’’’’’’’’’
p 

 
Ŗ "-¢*
# 
0’’’’’’’’’
 É
D__inference_model_17_layer_call_and_return_conditional_losses_151938BCDEFGH)./<=A¢>
7¢4
*'
inputs’’’’’’’’’
p

 
Ŗ "-¢*
# 
0’’’’’’’’’
 „
)__inference_model_17_layer_call_fn_151455xBCDEFGH)./<=F¢C
<¢9
/,
input_image’’’’’’’’’
p 

 
Ŗ " ’’’’’’’’’„
)__inference_model_17_layer_call_fn_151613xBCDEFGH)./<=F¢C
<¢9
/,
input_image’’’’’’’’’
p

 
Ŗ " ’’’’’’’’’ 
)__inference_model_17_layer_call_fn_151747sBCDEFGH)./<=A¢>
7¢4
*'
inputs’’’’’’’’’
p 

 
Ŗ " ’’’’’’’’’ 
)__inference_model_17_layer_call_fn_151776sBCDEFGH)./<=A¢>
7¢4
*'
inputs’’’’’’’’’
p

 
Ŗ " ’’’’’’’’’Š
J__inference_sequential_243_layer_call_and_return_conditional_losses_150990BK¢H
A¢>
41
conv2d_133_input’’’’’’’’’
p 

 
Ŗ "/¢,
%"
0’’’’’’’’’@
 Š
J__inference_sequential_243_layer_call_and_return_conditional_losses_150998BK¢H
A¢>
41
conv2d_133_input’’’’’’’’’
p

 
Ŗ "/¢,
%"
0’’’’’’’’’@
 Å
J__inference_sequential_243_layer_call_and_return_conditional_losses_151960wBA¢>
7¢4
*'
inputs’’’’’’’’’
p 

 
Ŗ "/¢,
%"
0’’’’’’’’’@
 Å
J__inference_sequential_243_layer_call_and_return_conditional_losses_151968wBA¢>
7¢4
*'
inputs’’’’’’’’’
p

 
Ŗ "/¢,
%"
0’’’’’’’’’@
 §
/__inference_sequential_243_layer_call_fn_150939tBK¢H
A¢>
41
conv2d_133_input’’’’’’’’’
p 

 
Ŗ ""’’’’’’’’’@§
/__inference_sequential_243_layer_call_fn_150982tBK¢H
A¢>
41
conv2d_133_input’’’’’’’’’
p

 
Ŗ ""’’’’’’’’’@
/__inference_sequential_243_layer_call_fn_151945jBA¢>
7¢4
*'
inputs’’’’’’’’’
p 

 
Ŗ ""’’’’’’’’’@
/__inference_sequential_243_layer_call_fn_151952jBA¢>
7¢4
*'
inputs’’’’’’’’’
p

 
Ŗ ""’’’’’’’’’@Ń
J__inference_sequential_244_layer_call_and_return_conditional_losses_151143CDEK¢H
A¢>
41
conv2d_134_input’’’’’’’’’@
p 

 
Ŗ ".¢+
$!
0’’’’’’’’’@@
 Ń
J__inference_sequential_244_layer_call_and_return_conditional_losses_151156CDEK¢H
A¢>
41
conv2d_134_input’’’’’’’’’@
p

 
Ŗ ".¢+
$!
0’’’’’’’’’@@
 Ę
J__inference_sequential_244_layer_call_and_return_conditional_losses_152015xCDEA¢>
7¢4
*'
inputs’’’’’’’’’@
p 

 
Ŗ ".¢+
$!
0’’’’’’’’’@@
 Ę
J__inference_sequential_244_layer_call_and_return_conditional_losses_152040xCDEA¢>
7¢4
*'
inputs’’’’’’’’’@
p

 
Ŗ ".¢+
$!
0’’’’’’’’’@@
 Ø
/__inference_sequential_244_layer_call_fn_151060uCDEK¢H
A¢>
41
conv2d_134_input’’’’’’’’’@
p 

 
Ŗ "!’’’’’’’’’@@Ø
/__inference_sequential_244_layer_call_fn_151130uCDEK¢H
A¢>
41
conv2d_134_input’’’’’’’’’@
p

 
Ŗ "!’’’’’’’’’@@
/__inference_sequential_244_layer_call_fn_151979kCDEA¢>
7¢4
*'
inputs’’’’’’’’’@
p 

 
Ŗ "!’’’’’’’’’@@
/__inference_sequential_244_layer_call_fn_151990kCDEA¢>
7¢4
*'
inputs’’’’’’’’’@
p

 
Ŗ "!’’’’’’’’’@@Š
J__inference_sequential_245_layer_call_and_return_conditional_losses_151301FGHJ¢G
@¢=
30
conv2d_135_input’’’’’’’’’@@
p 

 
Ŗ ".¢+
$!
0’’’’’’’’’  
 Š
J__inference_sequential_245_layer_call_and_return_conditional_losses_151314FGHJ¢G
@¢=
30
conv2d_135_input’’’’’’’’’@@
p

 
Ŗ ".¢+
$!
0’’’’’’’’’  
 Å
J__inference_sequential_245_layer_call_and_return_conditional_losses_152087wFGH@¢=
6¢3
)&
inputs’’’’’’’’’@@
p 

 
Ŗ ".¢+
$!
0’’’’’’’’’  
 Å
J__inference_sequential_245_layer_call_and_return_conditional_losses_152112wFGH@¢=
6¢3
)&
inputs’’’’’’’’’@@
p

 
Ŗ ".¢+
$!
0’’’’’’’’’  
 §
/__inference_sequential_245_layer_call_fn_151218tFGHJ¢G
@¢=
30
conv2d_135_input’’’’’’’’’@@
p 

 
Ŗ "!’’’’’’’’’  §
/__inference_sequential_245_layer_call_fn_151288tFGHJ¢G
@¢=
30
conv2d_135_input’’’’’’’’’@@
p

 
Ŗ "!’’’’’’’’’  
/__inference_sequential_245_layer_call_fn_152051jFGH@¢=
6¢3
)&
inputs’’’’’’’’’@@
p 

 
Ŗ "!’’’’’’’’’  
/__inference_sequential_245_layer_call_fn_152062jFGH@¢=
6¢3
)&
inputs’’’’’’’’’@@
p

 
Ŗ "!’’’’’’’’’  Ē
$__inference_signature_wrapper_151718BCDEFGH)./<=M¢J
¢ 
CŖ@
>
input_image/,
input_image’’’’’’’’’"?Ŗ<
:

conv2d_137,)

conv2d_137’’’’’’’’’ļ
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_151321R¢O
H¢E
C@
inputs4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
Ŗ "H¢E
>;
04’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
 Ē
1__inference_zero_padding2d_2_layer_call_fn_151327R¢O
H¢E
C@
inputs4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
Ŗ ";84’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’ļ
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_151334R¢O
H¢E
C@
inputs4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
Ŗ "H¢E
>;
04’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
 Ē
1__inference_zero_padding2d_3_layer_call_fn_151340R¢O
H¢E
C@
inputs4’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’
Ŗ ";84’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’’