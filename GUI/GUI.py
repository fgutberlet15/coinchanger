
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 29 13:15:29 2021

GUI for Corrosion + Wear Simulation of ancient coins

pretrained nets are required -> found under "./checkpoints"
"""

import tkinter as tk
from tkinter.filedialog import askopenfilename
import PIL
from PIL import ImageTk
import models
import numpy as np
import cv2
import tensorflow as tf


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.create_widgets()
        self.load_checkpoints()
       
    def create_widgets(self):
        """
        create Tkinter widgets
        """
        valuelist = [1,2,3,4,5]
        
        self.canvas = tk.Canvas(root)
        self.canvas.grid(column = 0,rowspan = 7,padx = 15, pady = 15)        
                
        self.load_btn = tk.Button(root, text = "Load Image",command = self.load)
        self.load_btn.grid(column = 1,row = 0)
        
        def valuecheck_a(value):
            newvalue = min(valuelist, key=lambda x:abs(x-float(value)))
            self.change()
            self.slider_a.set(newvalue)
        def valuecheck_k(value):
            newvalue = min(valuelist, key=lambda x:abs(x-float(value)))
            self.change()
            self.slider_k.set(newvalue)
            
        self.a_entry = tk.Entry(root,text = "Original Usage")
        self.a_entry.grid(column = 1,row = 1)
        
        self.k_entry = tk.Entry(root,text = "Original Corrosion")
        self.k_entry.grid(column = 1,row = 2)
        
        self.change_btn = tk.Button(root, text = "Change org Value",command = self.change_value)
        self.change_btn.grid(column = 1,row = 3)
            
        self.slider_a = tk.Scale(root, from_=min(valuelist), to=max(valuelist), command=valuecheck_a, orient="horizontal",label="Wear",length = 200)
        self.slider_a.grid(column = 1,row = 4)
        
        self.slider_k = tk.Scale(root, from_=min(valuelist), to=max(valuelist), command=valuecheck_k, orient="horizontal",label="Corrosion",length = 200)
        self.slider_k.grid(column = 1,row = 5)
        
        self.export = tk.Button(root,text="Export Current Image",command = self.export_pic)
        self.export.grid(column = 1,row = 6)
        
        self.quit = tk.Button(root, text="QUIT", fg="red",command=self.master.destroy)
        self.quit.grid(column = 1,row = 7)
        
    
        
    def load(self):
        """
        -starts if button "Load Image" is pressed
        -opens file-selection
        -predicts the current wear + corrosion state via discrimintive Nets
        -starts function to create all possible simulations 
        """
        self.path = askopenfilename()        
        img_array = cv2.imread(self.path)
        new_array = cv2.resize(img_array,(256, 256))
        img = new_array[...,::-1]
        img = ImageTk.PhotoImage(image = PIL.Image.fromarray(img))
        root.img = img
        self.img = self.canvas.create_image(256,256,image=img,anchor="se")
        
        
        input_img = tf.convert_to_tensor(new_array.reshape(1,256,256,3))
        input_img = tf.cast(input_img, tf.float32)
    
        input_img = (input_img / 127.5) - 1
        
        self.input_img = input_img
        
        a_pred = []
        k_pred = []
        
        a_pred.append(sum(sum(sum(self.Discs[0].predict(input_img)))))
        a_pred.append(sum(sum(sum(self.Discs[1].predict(input_img)))))
        a_pred.append(sum(sum(sum(self.Discs[2].predict(input_img)))))
        a_pred.append(sum(sum(sum(self.Discs[3].predict(input_img)))))
        a_pred.append(sum(sum(sum(self.Discs[4].predict(input_img)))))
        
        k_pred.append(sum(sum(sum(self.Discs[5].predict(input_img)))))
        k_pred.append(sum(sum(sum(self.Discs[6].predict(input_img)))))
        k_pred.append(sum(sum(sum(self.Discs[7].predict(input_img)))))
        k_pred.append(sum(sum(sum(self.Discs[8].predict(input_img)))))
        k_pred.append(sum(sum(sum(self.Discs[9].predict(input_img)))))
        
        a_org = np.argmax(a_pred)+1
        k_org = np.argmax(k_pred)+1
        
        self.a_entry.delete(0)
        self.a_entry.insert(0,str(a_org))
        self.k_entry.delete(0)
        self.k_entry.insert(0,str(k_org))
        
        self.slider_a.set(a_org)
        self.slider_k.set(k_org)
        
        #create simulations
        self.create_images(a_org,k_org)
                
    def change(self):
        """
        -gets called if a value of a slider gets changed
        -prints the requested image
        """
        a = self.slider_a.get()
        k = self.slider_k.get()
        
        img = self.display_list[a-1][k-1]*0.5+0.5
        img = img[...,::-1]
        img = PIL.Image.fromarray((np.array(img) * 255).astype(np.uint8))
        img = ImageTk.PhotoImage(image = img)
        root.img = img
        self.canvas.itemconfig(self.img,image=img)
        
        
        
    def change_value(self):
        """
        -starts if button "Change org Value" gets pressed
        -changes the original state of attributes to the written value
        """
        a_org = int(self.a_entry.get())
        k_org = int(self.k_entry.get())
        self.create_images(a_org,k_org)
        self.slider_a.set(a_org)
        self.slider_k.set(k_org)
            
    def create_images(self,a_org,k_org):
        """
        -simulates all 25 possible images starting from one
        -image gets handed through all generators to create all stages -> 5 images per attribute
        """
        
        self.display_list_a = []
        self.display_list_k = []
        
        #Wear Simulation
        if(a_org == 1):
            self.display_list_a.append(self.input_img)
            x = self.Gens_Wear[0](self.input_img)
            self.display_list_a.append(x)
            x = self.Gens_Wear[2](x)
            self.display_list_a.append(x)
            x = self.Gens_Wear[4](x)
            self.display_list_a.append(x)
            x = self.Gens_Wear[6](x)
            self.display_list_a.append(x)
            
        elif(a_org == 2):
            x = self.Gens_Wear[1](self.input_img)
            self.display_list_a.append(x)
            self.display_list_a.append(self.input_img)
            x = self.Gens_Wear[2](self.input_img)
            self.display_list_a.append(x)
            x = self.Gens_Wear[4](x)
            self.display_list_a.append(x)
            x = self.Gens_Wear[6](x)
            self.display_list_a.append(x)
        
        elif(a_org == 3):
            x = self.Gens_Wear[3](self.input_img)
            y = self.Gens_Wear[1](x)
            self.display_list_a.append(y)
            self.display_list_a.append(x)
            self.display_list_a.append(self.input_img)
            x = self.Gens_Wear[4](self.input_img)
            self.display_list_a.append(x)
            x = self.Gens_Wear[6](x)
            self.display_list_a.append(x)
            
        elif(a_org == 4):
            x = self.Gens_Wear[5](self.input_img)
            y = self.Gens_Wear[3](x)
            z = self.Gens_Wear[1](y)
            self.display_list_a.append(z)
            self.display_list_a.append(y)
            self.display_list_a.append(x)
            self.display_list_a.append(self.input_img)
            x = self.Gens_Wear[6](x)
            self.display_list_a.append(x)
            
        elif(a_org == 5):
            x = self.Gens_Wear[7](self.input_img)
            y = self.Gens_Wear[5](x)
            z = self.Gens_Wear[3](y)
            zz = self.Gens_Wear[1](z)
            self.display_list_a.append(zz)
            self.display_list_a.append(z)
            self.display_list_a.append(y)
            self.display_list_a.append(x)
            self.display_list_a.append(self.input_img)
            
        ##################################################    
        
        #Corrosion Simulation
        if(k_org == 1):
            self.display_list_k.append(self.input_img)
            x = self.Gens_Corrosion[0](self.input_img)
            self.display_list_k.append(x)
            x = self.Gens_Corrosion[2](x)
            self.display_list_k.append(x)
            x = self.Gens_Corrosion[4](x)
            self.display_list_k.append(x)
            x = self.Gens_Corrosion[6](x)
            self.display_list_k.append(x)
            
        elif(k_org == 2):
            x = self.Gens_Corrosion[1](self.input_img)
            self.display_list_k.append(x)
            self.display_list_k.append(self.input_img)
            x = self.Gens_Corrosion[2](self.input_img)
            self.display_list_k.append(x)
            x = self.Gens_Corrosion[4](x)
            self.display_list_k.append(x)
            x = self.Gens_Corrosion[6](x)
            self.display_list_k.append(x)
        
        elif(k_org == 3):
            x = self.Gens_Corrosion[3](self.input_img)
            y = self.Gens_Corrosion[1](x)
            self.display_list_k.append(y)
            self.display_list_k.append(x)
            self.display_list_k.append(self.input_img)
            x = self.Gens_Corrosion[4](self.input_img)
            self.display_list_k.append(x)
            x = self.Gens_Corrosion[6](x)
            self.display_list_k.append(x)
            
        elif(k_org == 4):
            x = self.Gens_Corrosion[5](self.input_img)
            y = self.Gens_Corrosion[3](x)
            z = self.Gens_Corrosion[1](y)
            self.display_list_k.append(z)
            self.display_list_k.append(y)
            self.display_list_k.append(x)
            self.display_list_k.append(self.input_img)
            x = self.Gens_Corrosion[6](x)
            self.display_list_k.append(x)
            
        elif(k_org == 5):
            x = self.Gens_Corrosion[7](self.input_img)
            y = self.Gens_Corrosion[5](x)
            z = self.Gens_Corrosion[3](y)
            zz = self.Gens_Corrosion[1](z)
            self.display_list_k.append(zz)
            self.display_list_k.append(z)
            self.display_list_k.append(y)
            self.display_list_k.append(x)
            self.display_list_k.append(self.input_img)
            
            
        self.display_list = []    
        
        #create 5x5 Array with all possible images
        #if only 1 attribute is changed show original simulation
        #if 2 attributes changed blend the 2 simulations with "cv2.addWighted" with 50% per image
        for i in range(5):
            self.display_list.append([])
            for j in range(5):
                x = self.display_list_a[i]
                y = self.display_list_k[j]
                
                if(i == a_org-1):
                    self.display_list[i].append(y[0])
                elif(j == k_org-1):
                    self.display_list[i].append(x[0])
                else:
                    dst = cv2.addWeighted(np.array(x[0]),0.5,np.array(y[0]),0.5,0)
                    
                    self.display_list[i].append(dst)
        
        
        
    def export_pic(self):
        """
        -starts if button "Export Current Image" gets pressed
        -exports current shown image to "./Export" folder
        """
        pic_path = self.path
        a = self.slider_a.get()
        k = self.slider_k.get()
        
        name = ""
        
        for i in range(len(pic_path)):
            if(pic_path[len(pic_path)-1] == "."):
                pic_path = pic_path[:-1]
                for j in range(len(pic_path)):
                    if(pic_path[len(pic_path)-1-j] == "/"):
                        name = name[::-1]
                        break
                    else:
                        name += pic_path[len(pic_path)-1-j]
            else:
                pic_path = pic_path[:-1]
                
        path = "./Export"
        new_path = path +"/"+name+"_A_" + str(a) + "_K_" + str(k)+".png"
        
        img = self.display_list[a-1][k-1]*0.5+0.5
        img = img[...,::-1]
        img = PIL.Image.fromarray((np.array(img) * 255).astype(np.uint8))
        print("saved to "+new_path)
        img.save(new_path)
    
    def load_checkpoints(self):
        """
        load the pre-trained nets
        """
        path = ["A_1_2","A_2_3","A_3_4","A_4_5","K_1_2","K_2_3","K_3_4","K_4_5"]
        self.Gens_Wear = []
        self.Gens_Corrosion = []
        self.Discs = []
        
        for i in range(4):
            self.Gens_Wear.append(tf.keras.models.load_model("./checkpoints/Gens_A/G/"+path[i],compile=False))
            self.Gens_Wear.append(tf.keras.models.load_model("./checkpoints/Gens_A/F/"+path[i],compile=False))
            
            self.Gens_Corrosion.append(tf.keras.models.load_model("./checkpoints/Gens_K/G/"+path[4+i],compile=False))
            self.Gens_Corrosion.append(tf.keras.models.load_model("./checkpoints/Gens_K/F/"+path[4+i],compile=False))
        
        for i in range(8):
            self.Discs.append(tf.keras.models.load_model("./checkpoints/Discs/"+path[i],compile=False))
            if(i == 3 or i == 7):
                #"./checkpoints/Discs/"+path[i]+"_Y"
                self.Discs.append(tf.keras.models.load_model("./checkpoints/Discs/"+path[i]+"_Y",compile=False))
               
####################################################################################################################################
        
#Start mainloop
root = tk.Tk()
app = Application(master=root)
app.mainloop()